package daos.mysqlimpl;

import daos.PageDao;
import daos.mysqlimpl.mappermodels.IndexMapperModel;
import enums.LoadStatus;
import javassist.NotFoundException;
import models.*;
import models.exceptions.AlreadyExistsException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.dao.DuplicateKeyException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Repository;
import play.Logger;

import javax.sql.DataSource;
import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.*;

/**
 * Created by adippel on 20.10.2015.
 */
@Repository("pageDaoMySql")
public class PageDaoMysql implements PageDao {

    private static final String PAGE_TABLE = "page";
    private static final String SUBPAGES_TABLE = "subpages";
    private static final String PAGE_X_IMAGE_TABLE = "page_x_image";
    private static final String PAGE_X_CSS_TABLE = "page_x_css";
    private static final String PAGE_X_JS_TABLE = "page_x_js";
    private static final String INDEX_REPORT_TABLE = "index_report";
    private DataSource dataSource;
    private JdbcTemplate jdbcTemplate;

    @Autowired
    public void setDataSource(DataSource dataSource) {
        this.dataSource = dataSource;
        this.jdbcTemplate = new JdbcTemplate(dataSource);
    }



    @Override
    public void insert(Page page) throws AlreadyExistsException, IOException {
        try {
            String insertPageQuery = "insert into " + PAGE_TABLE+" (url,status) values (?, ?)";
            jdbcTemplate.update(insertPageQuery, page.getUrl().toString(), page.getStatus().name());
            Logger.info("Page "+page.getUrl()+" is saved in db");
        }catch (DuplicateKeyException e){
            Logger.warn("Page already exists in db " + page.getUrl());
            throw new AlreadyExistsException("Page already in database");
        }catch (DataAccessException e){
            throw new IOException(e);
        }
    }

    @Override
    public void updateFullContent(Page page) throws IOException {
        Logger.info("Updating content for " + page.getUrl());
        String updatePageQuery = "update " + PAGE_TABLE + " set status=?, html=?, title_content=?, body_content=?, last_update=? where url=?";
        PageContent pageContent = page.getContent();
        try {
            jdbcTemplate.update(updatePageQuery,
                    page.getStatus().name(),
                    pageContent.getHtml(),
                    pageContent.getTitleContent(),
                    pageContent.getBodyContent(),
                    pageContent.getLastUpdate(),
                    page.getUrl().toString());
        }catch (DataAccessException e){
            throw new IOException(e);
        }

        for(URL suburl: page.getSubUrls()) {
                insertSubUrl(page.getUrl(),suburl);
        }

        insertSourceRefs(page.getUrl(), pageContent.getImages(), PAGE_X_IMAGE_TABLE);
        insertSourceRefs(page.getUrl(), pageContent.getJsList(), PAGE_X_JS_TABLE);
        insertSourceRefs(page.getUrl(), pageContent.getCssList(), PAGE_X_CSS_TABLE);

    }

    private void insertSourceRefs(URL url, Set<String> sources, String tableName) throws IOException {
        String insertSubpages = "insert into " + tableName + " (url, source) values (?, ?)";
        for(String source : sources) {
            try {
                jdbcTemplate.update(insertSubpages, url.toString(), source);
            }catch (DataAccessException e){
                throw new IOException(e);
            }
        }
    }

    private void insertSubUrl(URL url, URL suburl) throws IOException {
        try {
            String insertSubpages = "insert into " + SUBPAGES_TABLE + " (url, suburl) values (?, ?)";
            jdbcTemplate.update(insertSubpages, url.toString(), suburl.toString());
        }catch (DataAccessException e){
            throw new IOException(e);
        }
    }

    @Override
    public void updateStatus(URL url, LoadStatus loadStatus) throws IOException {
        Logger.debug("Updating status for " + url + " "+ loadStatus);
        String updateStatusQuery = "update "+ PAGE_TABLE +" set status=? where url=?";
        try {
            jdbcTemplate.update(updateStatusQuery, loadStatus.toString(), url.toString());
        }catch (DataAccessException e){
            throw new IOException(e);
        }
    }

    @Override
    public void updateIndexReport(Page page) throws IOException {
        Logger.debug("Updating index report for " + page.getUrl());
        updateStatus(page.getUrl(), page.getStatus());
        PageIndexReport pageIndexReport = page.getIndexReport();
        Map<String, Integer> indexMap = pageIndexReport.getIndexMap();
        for(Map.Entry<String, Integer> mapEntry : indexMap.entrySet()) {
            String insertIndexReport = "insert into "+ INDEX_REPORT_TABLE +" (url, page_index, count) values (?,?,?)";
            try {
                jdbcTemplate.update(insertIndexReport,
                        page.getUrl().toString(),
                        mapEntry.getKey(),
                        mapEntry.getValue()
                );
            }catch (DataAccessException e){
                throw new IOException(e);
            }
        }
    }

    @Override
    public LoadStatus getStatus(URL url) throws NotFoundException, IOException {
        Logger.debug("Getting status for url "+ url);
        String selectStatusQuery = "select * from "+ PAGE_TABLE +" where url=?";
        try {
            List<Page> pages = jdbcTemplate.query(selectStatusQuery, new PageMapper(), url.toString());
            if(pages.isEmpty()){
                throw new NotFoundException("Page with url " + url +" not found");
            }else{
                return pages.get(0).getStatus();
            }
        }catch (DataAccessException e){
            Logger.warn(e.getMessage());
            throw new IOException(e);
        }
    }

    @Override
    public PageIndexReport getIndexReport(URL url) throws NotFoundException, IOException {
        String selectIndexReportsQuery = "select * from "+ INDEX_REPORT_TABLE +" where url=?";
        try {
            List<IndexMapperModel> indexes = jdbcTemplate.query(selectIndexReportsQuery, new Object[]{url.toString()}, new IndexReportMapper());
            if (indexes.isEmpty()) {
                throw new NotFoundException("Index report for url " + url + " not found");
            }
            Map<String, Integer> indexMap = new HashMap<>();
            for (IndexMapperModel index : indexes) {
                indexMap.put(index.getIndex(), index.getCount());
            }
            PageIndexReport pageIndexReport = new PageIndexReport();
            pageIndexReport.setIndexMap(indexMap);
            return pageIndexReport;
        }catch (DataAccessException e){
            throw new IOException(e);
        }

    }

    @Override
    public Page getPageContentAndSubUrls(URL url) throws IOException {
        String selectPageQuery = "select * from "+ PAGE_TABLE +" where url =?";
        try {
            List<Page> pages = jdbcTemplate.query(selectPageQuery, new Object[]{url.toString()}, new PageMapper());
            if (pages.isEmpty()) {
                return null;
            }
            Page page = pages.get(0);

            Set<URL> subUrlSet = getSubUrls(url);

            page.setSubUrls(subUrlSet);

            PageContent pageContent = page.getContent();

            pageContent.setImages(getSources(PAGE_X_IMAGE_TABLE, url));
            pageContent.setJsList(getSources(PAGE_X_JS_TABLE, url));
            pageContent.setCssList(getSources(PAGE_X_CSS_TABLE, url));

            page.setContent(pageContent);

            return page;
        }catch (DataAccessException e){
            Logger.warn(e.getMessage());
            throw new IOException(e);
        }
    }

    private Set<String> getSources(String tableName, URL url){
        try {
            String selectResourcesQuery = "select source from " + tableName + " where url=?";
            List<String> sources = jdbcTemplate.query(selectResourcesQuery, new Object[]{url.toString()}, new RowMapper<String>() {
                @Override
                public String mapRow(ResultSet resultSet, int i) throws SQLException {
                    String source = resultSet.getString("source");
                    return source;
                }

            });
            return new HashSet<>(sources);
        }catch (DataAccessException e){
            Logger.warn(e.getMessage());
            return Collections.emptySet();
        }
    }

    private Set<URL> getSubUrls(URL url){
        String selectSubUrlsQuery = "select suburl from "+ SUBPAGES_TABLE +" where url=?";
        try {
            List<URL> subUrls = jdbcTemplate.query(selectSubUrlsQuery, new Object[]{url.toString()}, new RowMapper<URL>() {
                @Override
                public URL mapRow(ResultSet resultSet, int i) throws SQLException {
                    try {
                        URL subUrl = new URL(resultSet.getString("suburl"));
                        return subUrl;
                    } catch (MalformedURLException e) {
                        e.printStackTrace();
                    }
                    return null;
                }
            });
            Set<URL> subUrlSet = new HashSet<>(subUrls);
            return subUrlSet;
        }catch (DataAccessException e){
            Logger.warn(e.getMessage());
            return Collections.emptySet();
        }

    }

    @Override
    public Page findPageAndRemove(URL url) throws IOException {
        try {
            Logger.info("Deleting page " + url);
            Page page = getPageContentAndSubUrls(url);
            String deletePageQuery = "delete from " + PAGE_TABLE + " where url=?";
            jdbcTemplate.update(deletePageQuery, url.toString());

            String deleteSubPagesQuery = "delete from " + SUBPAGES_TABLE + " where url=?";
            jdbcTemplate.update(deleteSubPagesQuery, url.toString());

            return page;
        }catch (DataAccessException e){
            throw new IOException(e);
        }

    }

    @Override
    public long getPageCountByStatus(LoadStatus loadStatus) throws IOException {
        try {
            String countPagesByStatus = "select count(*) as count from " + PAGE_TABLE + " where status=?";

            Long count = jdbcTemplate.queryForObject(countPagesByStatus, new Object[]{loadStatus.name()}, new RowMapper<Long>() {
                @Override
                public Long mapRow(ResultSet resultSet, int i) throws SQLException {
                    return resultSet.getLong("count");
                }
            });

            return count;
        }catch (DataAccessException e){
            Logger.warn(e.getMessage());
            throw new IOException(e);
        }
    }

    @Override
    public List<Page> getPartialPagesWithTextContentByStatus(LoadStatus loadStatus, int skip, int limit) throws IOException {
        try {
            String getPartialPages = "select * from " + PAGE_TABLE + " where status=? limit ?,?";
            List<Page> pages = jdbcTemplate.query(getPartialPages, new Object[]{loadStatus.name(), skip, limit}, new PageMapper());
            return pages;
        }catch (DataAccessException e){
            Logger.warn(e.getMessage());
            throw new IOException(e);
        }
    }

    @Override
    public List<Page> getFilteredPageList(FilterParameter filterParameter) throws IOException {
        List<Object> params = new ArrayList<>();
        StringBuilder getFilteredPagesQuery = new StringBuilder();
        getFilteredPagesQuery.append("select * from "+ PAGE_TABLE +" where");

        if(filterParameter.getUrlParameter()!=null){
            String filteredUrl = filterParameter.getUrlParameter();
            getFilteredPagesQuery.append(" (url like ?) and");
            params.add("%"+filteredUrl+"%");
        }
        if(filterParameter.getTitleParameter()!=null){
            String titleParameter = filterParameter.getTitleParameter();
            getFilteredPagesQuery.append(" (title_content like ?) and");
            params.add("%"+titleParameter+"%");
        }
        if(filterParameter.getBodyParameter()!=null){
            String bodyParameter = filterParameter.getBodyParameter();
            getFilteredPagesQuery.append(" (body_content like ?) and");
            params.add("%"+bodyParameter+"%");

        }
        if(filterParameter.getDateParameter()!=null){
            Date dateParameter = filterParameter.getDateParameter();
            getFilteredPagesQuery.append(" (last_update >?) and" );
            params.add(dateParameter);
        }
        getFilteredPagesQuery.append(" (true)");

        Object[] paramsArray = params.toArray();
        try {
            List<Page> pages = jdbcTemplate.query(getFilteredPagesQuery.toString(), paramsArray, new PageMapper());
            return pages;
        }catch (DataAccessException e){
            throw new IOException(e);
        }
    }

    @Override
    public long getPageCount() throws IOException {
        try {
            String countPagesByStatus = "select count(*) as count from " + PAGE_TABLE;

            Long count = jdbcTemplate.queryForObject(countPagesByStatus, new RowMapper<Long>() {
                @Override
                public Long mapRow(ResultSet resultSet, int i) throws SQLException {
                    return resultSet.getLong("count");
                }
            });

            return count;
        }catch (DataAccessException e){
            Logger.warn(e.getMessage());
            throw new IOException(e);
        }
    }

    @Override
    public void clear() throws IOException {
        try {
            Logger.info("Deleting all pages");
            String deletePageQuery = "delete from " + PAGE_TABLE;
            jdbcTemplate.update(deletePageQuery);

            String deleteSubPageQuery = "delete from " + SUBPAGES_TABLE;
            jdbcTemplate.update(deleteSubPageQuery);
        }catch (DataAccessException e){
            throw new IOException(e);
        }
    }


    class PageMapper implements RowMapper<Page> {
        @Override
        public Page mapRow(ResultSet rs, int rowNum) throws SQLException {
            Page page = new Page();

            try {
                page.setUrl(new URL(rs.getString("url")));
            } catch (MalformedURLException e) {
                e.printStackTrace();
            }
            page.setStatus(LoadStatus.valueOf(rs.getString("status")));
            PageContent pageContent = new PageContent();
            pageContent.setHtml(rs.getString("html"));
            pageContent.setBodyContent(rs.getString("body_content"));
            pageContent.setTitleContent(rs.getString("title_content"));
            pageContent.setLastUpdate(rs.getDate("last_update"));
            page.setContent(pageContent);
            return page;
        }
    }

    class ImageMapper implements RowMapper<ImgResource>{
        @Override
        public ImgResource mapRow(ResultSet resultSet, int i) throws SQLException {
            ImgResource imgResource = new ImgResource();
            imgResource.setSource(resultSet.getString("source"));
            imgResource.setByteContent(resultSet.getBytes("content"));
            return  imgResource;
        }
    }

    class CssMapper implements RowMapper<CssResource>{
        @Override
        public CssResource mapRow(ResultSet resultSet, int i) throws SQLException {
            CssResource cssResource = new CssResource();
            cssResource.setSource(resultSet.getString("source"));
            cssResource.setContent(resultSet.getString("content"));
            return cssResource;
        }
    }

    class JsMapper implements RowMapper<JsResource>{
        @Override
        public JsResource mapRow(ResultSet resultSet, int i) throws SQLException {
            JsResource jsResource = new JsResource();
            jsResource.setSource(resultSet.getString("source"));
            jsResource.setContent(resultSet.getString("content"));
            return jsResource;
        }
    }

    class IndexReportMapper implements RowMapper<IndexMapperModel>{
        @Override
        public IndexMapperModel mapRow(ResultSet resultSet, int i) throws SQLException {
            IndexMapperModel indexMapperModel = new IndexMapperModel();
            indexMapperModel.setIndex(resultSet.getString("page_index"));
            indexMapperModel.setCount(resultSet.getInt("count"));
            return indexMapperModel;
        }
    }
}
