package daos.mysqlimpl;

import daos.IndexDao;
import models.Index;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Repository;
import play.Logger;

import javax.sql.DataSource;
import java.io.IOException;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

/**
 * Created by adippel on 11/20/15.
 */
@Repository("indexDaoMySql")
public class IndexDaoMySql implements IndexDao {
    private static final String INDEX_TABLE = "page_indexes";
    private DataSource dataSource;
    private JdbcTemplate jdbcTemplate;

    @Autowired
    public void setDataSource(DataSource dataSource) {
        this.dataSource = dataSource;
        this.jdbcTemplate = new JdbcTemplate(dataSource);
    }
    @Override
    public void save(Index index) throws IOException {
        String saveIndexQuery = "insert into "+ INDEX_TABLE+" (page_index) value (?)";
        try {
            jdbcTemplate.update(saveIndexQuery, index.getIndex());
        }catch(DataAccessException e){
            throw new IOException(e.getMessage(), e);
        }
    }

    @Override
    public void delete(Index index) throws IOException {
        try {
            String removeIndexQuery = "delete from " + INDEX_TABLE + " where page_index = ?";
            jdbcTemplate.update(removeIndexQuery, index.getIndex());
        }catch (DataAccessException e){
            Logger.warn(e.getMessage());
            throw new IOException(e.getMessage(), e);
        }
    }

    @Override
    public Set<Index> findAll() throws IOException {
        String selectAllIndexes = "select * from "+ INDEX_TABLE;
        try {
            List<Index> indexes = jdbcTemplate.query(selectAllIndexes, new IndexMapper());
            if (indexes.isEmpty()) {
                return Collections.emptySet();
            }
            Set<Index> indexSet = new HashSet<>(indexes);
            return indexSet;
        }catch (DataAccessException e){
            Logger.warn(e.getMessage());
            throw new IOException(e.getMessage(), e);

        }
    }


    @Override
    public void clear() throws IOException {
        try {
            String clearQuery = "delete from " + INDEX_TABLE;
            jdbcTemplate.update(clearQuery);
        }catch (DataAccessException e){
            throw new IOException(e.getMessage());
        }
    }

    class IndexMapper implements RowMapper<Index>{

        @Override
        public Index mapRow(ResultSet resultSet, int i) throws SQLException {
            Index index = new Index();
            index.setIndex(resultSet.getString("page_index"));
            return index;
        }
    }
}
