package daos.mysqlimpl;


import daos.JsDao;
import models.JsResource;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Repository;
import org.springframework.util.CollectionUtils;
import play.Logger;

import javax.sql.DataSource;
import java.io.IOException;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

/**
 * Created by adippel on 11/20/15.
 */
@Repository("jsDaoMySql")
public class JsDaoMySql implements JsDao {
    private static final String JS_TABLE = "js";
    private static final String PAGE_X_JS_TABLE = "page_x_js";
    private DataSource dataSource;
    private JdbcTemplate jdbcTemplate;

    @Autowired
    public void setDataSource(DataSource dataSource) {
        this.dataSource = dataSource;
        this.jdbcTemplate = new JdbcTemplate(dataSource);
    }

    @Override
    public JsResource find(String jsSource) throws IOException {
        try {
            String selectJsResource = "select source, content from " + JS_TABLE + " where source = ?";
            List<JsResource> jsResources = jdbcTemplate.query(selectJsResource, new JsResourceMapper(), jsSource);
            return (jsResources.isEmpty() ? null:jsResources.get(0));
        }catch (DataAccessException e){
            Logger.warn(e.getMessage());
            throw new IOException(e.getMessage(), e);
        }
    }

    @Override
    public void delete(String jsSource) throws IOException {
        try {
            String getJsRefs = "select url from " + PAGE_X_JS_TABLE + " where source=?";
            List<String> urls = jdbcTemplate.query(getJsRefs, new Object[]{jsSource}, (resultSet, i) -> {
                return resultSet.getString("url");
            });

            if(CollectionUtils.isEmpty(urls)) {
                String removeJsQuery = "delete from " + JS_TABLE + " where source = ?";
                jdbcTemplate.update(removeJsQuery, jsSource);
            }
        }catch (DataAccessException e){
            Logger.warn(e.getMessage());
            throw new IOException(e.getMessage(), e);
        }
    }

    @Override
    public void save(JsResource jsResource) throws IOException {
        try {
            String saveJsQuery = "insert into " + JS_TABLE + " (source, content) values (?,?)";
            jdbcTemplate.update(saveJsQuery, jsResource.getSource(), jsResource.getContent());
        }catch (DataAccessException e){
            Logger.warn(e.getMessage());
            throw new IOException(e.getMessage(), e);
        }
    }

    @Override
    public void clear() throws IOException {
        try {
            String clearQuery = "delete from " + JS_TABLE;
            jdbcTemplate.update(clearQuery);
        }catch (DataAccessException e){
            throw new IOException(e.getMessage());
        }
    }


    class JsResourceMapper implements RowMapper<JsResource>{

        @Override
        public JsResource mapRow(ResultSet resultSet, int i) throws SQLException {
            JsResource jsResource = new JsResource();
            jsResource.setSource(resultSet.getString("source"));
            jsResource.setContent(resultSet.getString("content"));
            return jsResource;
        }
    }
}
