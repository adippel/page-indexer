package daos.mysqlimpl.mappermodels;

/**
 * Created by adippel on 11/19/15.
 */
public class IndexMapperModel {
    private String index;
    private int count;

    public String getIndex() {
        return index;
    }

    public void setIndex(String index) {
        this.index = index;
    }

    public int getCount() {
        return count;
    }

    public void setCount(int count) {
        this.count = count;
    }
}
