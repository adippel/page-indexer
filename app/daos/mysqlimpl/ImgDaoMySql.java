package daos.mysqlimpl;

import daos.ImgDao;
import models.ImgResource;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Repository;
import org.springframework.util.CollectionUtils;
import play.Logger;

import javax.sql.DataSource;
import java.io.IOException;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

/**
 * Created by adippel on 11/20/15.
 */
@Repository("imgDaoMySql")
public class ImgDaoMySql implements ImgDao {
    private static final String IMAGE_TABLE = "image";
    private static final String PAGE_X_IMAGE_TABLE = "page_x_image";
    private DataSource dataSource;
    private JdbcTemplate jdbcTemplate;

    @Autowired
    public void setDataSource(DataSource dataSource) {
        this.dataSource = dataSource;
        this.jdbcTemplate = new JdbcTemplate(dataSource);
    }

    @Override
    public ImgResource find(String imgSource) throws IOException {
        String selectImageResource = "select source, content from "+ IMAGE_TABLE +" where source=?";
        try {
            List<ImgResource> imgResources = jdbcTemplate.query(selectImageResource, new ImgResourceMapper(), imgSource);
            return (imgResources.isEmpty() ? null : imgResources.get(0));
        }catch (DataAccessException e){
            Logger.warn(e.getMessage());
            throw new IOException(e.getMessage(), e);
        }
    }

    @Override
    public void delete(String imgSource) throws IOException {
        try {
            String getImageRefs = "select url from " + PAGE_X_IMAGE_TABLE + " where source = ?";
            List<String> urls = jdbcTemplate.query(getImageRefs, new Object[]{imgSource}, (resultSet, i) -> {
                return resultSet.getString("url");
            });

            if(CollectionUtils.isEmpty(urls)) {
                String removeImageQuery = "delete from " + IMAGE_TABLE + " where source =?";
                jdbcTemplate.update(removeImageQuery, imgSource);
            }

        }catch (DataAccessException e){
            Logger.warn(e.getMessage());
            throw new IOException(e.getMessage(), e);
        }
    }

    @Override
    public void save(ImgResource imgResource) throws IOException {
        try {
            String saveImageQuery = "insert into " + IMAGE_TABLE + " (source, content) values (?,?)";
            jdbcTemplate.update(saveImageQuery, imgResource.getSource(), imgResource.getByteContent());
        }catch (DataAccessException e){
            Logger.warn(e.getMessage());
            throw new IOException(e.getMessage());
        }
    }

    @Override
    public void clear() throws IOException {
        try {
            String clearQuery = "delete from " + IMAGE_TABLE;
            jdbcTemplate.update(clearQuery);
        }catch (DataAccessException e){
            throw new IOException(e.getMessage());
        }
    }

    class ImgResourceMapper implements RowMapper<ImgResource>{
        @Override
        public ImgResource mapRow(ResultSet resultSet, int i) throws SQLException {
            ImgResource imgResource = new ImgResource();
            imgResource.setSource(resultSet.getString("source"));
            imgResource.setByteContent(resultSet.getBytes("content"));
            return imgResource;
        }
    }
}
