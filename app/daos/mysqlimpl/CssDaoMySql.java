package daos.mysqlimpl;

import daos.CssDao;
import models.CssResource;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Repository;
import org.springframework.util.CollectionUtils;
import play.Logger;

import javax.sql.DataSource;
import java.io.IOException;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

/**
 * Created by adippel on 11/20/15.
 */
@Repository("cssDaoMySql")
public class CssDaoMySql implements CssDao {

    private static final String CSS_TABLE = "css";
    private static final String PAGE_X_CXX_TABLE = "page_x_css";
    private DataSource dataSource;
    private JdbcTemplate jdbcTemplate;

    @Autowired
    public void setDataSource(DataSource dataSource) {
        this.dataSource = dataSource;
        this.jdbcTemplate = new JdbcTemplate(dataSource);
    }
    @Override
    public CssResource find(String cssSource) throws IOException {
        try {
            String selectCssResource = "select source, content from " + CSS_TABLE + " where source = ?";
            List<CssResource> cssResources = jdbcTemplate.query(selectCssResource, new CssResourceMapper(), cssSource);
            return (cssResources.isEmpty() ? null : cssResources.get(0));
        }catch(DataAccessException e){
            Logger.warn(e.getMessage());
            throw new IOException(e.getMessage(), e);
        }
    }

    @Override
    public void delete(String cssSource) throws IOException {
        try {
            String getResourcesRefs = "select url from "+ PAGE_X_CXX_TABLE + " where source = ?";
            List<String> urls = jdbcTemplate.query(getResourcesRefs, new Object[]{cssSource}, (resultSet, i) -> {
                return resultSet.getString("url");
            });
            if(CollectionUtils.isEmpty(urls)) {
                String removeCssQuery = "delete from " + CSS_TABLE + " where source = ?";
                jdbcTemplate.update(removeCssQuery, cssSource);
            }
        }catch (DataAccessException e){
            Logger.warn(e.getMessage());
            throw new IOException(e.getMessage(), e);
        }
    }

    @Override
    public void save(CssResource cssResource) throws IOException {
        try {
            String saveCssQuery = "insert into " + CSS_TABLE + " (source, content) values (?,?)";
            jdbcTemplate.update(saveCssQuery, cssResource.getSource(), cssResource.getContent());

        }catch(DataAccessException e){
            Logger.warn(e.getMessage());
            throw new IOException(e.getMessage());
        }
    }

    @Override
    public void clear() throws IOException {
        try {
            String clearQuery = "delete from " + CSS_TABLE;
            jdbcTemplate.update(clearQuery);
        }catch (DataAccessException e){
            throw new IOException(e.getMessage());
        }
    }

    class CssResourceMapper implements RowMapper<CssResource>{

        @Override
        public CssResource mapRow(ResultSet resultSet, int i) throws SQLException {
            CssResource cssResource = new CssResource();
            cssResource.setSource(resultSet.getString("source"));
            cssResource.setContent(resultSet.getString("content"));
            return cssResource;
        }
    }
}
