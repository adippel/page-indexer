package daos.mongoImpl;

import daos.ImgDao;
import domains.ImgResourceDomain;
import domains.PageXResourceDomain;
import models.ImgResource;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.convert.converter.Converter;
import org.springframework.dao.DuplicateKeyException;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.stereotype.Repository;
import play.Logger;

import java.io.IOException;
import java.util.List;

/**
 * Created by adippel on 29.10.2015.
 */
@Repository("imgDaoMongo")
public class ImgDaoMongo implements ImgDao {
    private static final String COLLECTION_NAME = "image";
    private static final String PAGE_XREF_COLLECTION_NAME = "pageImageXRef";
    @Autowired
    Converter<ImgResource, ImgResourceDomain> imgResourceImgResourceDomainConverter;
    @Autowired
    Converter<ImgResourceDomain, ImgResource> imgResourceDomainImgResourceConverter;
    @Autowired
    MongoTemplate mongoTemplate;

    @Override
    public ImgResource find(final String imgSource){
        Logger.debug("Searching image " + imgSource + " in db");
        final Query findResourceBySource  = new Query(Criteria.where("_id").is(imgSource));
        final List<ImgResourceDomain> imgResourceDomain = mongoTemplate.find(findResourceBySource, ImgResourceDomain.class, COLLECTION_NAME);
        if(imgResourceDomain == null || imgResourceDomain.isEmpty()){
            Logger.debug("images found 0 " + " in db");
            return null;
        }
        Logger.debug("images found " + imgResourceDomain.size() + " in db");
        return imgResourceDomainImgResourceConverter.convert(imgResourceDomain.get(0));
    }

    @Override
    public void delete(final String imgSource){
        Logger.info("Deleting image");
        final Query findRefBySource = new Query((Criteria.where("source").is(imgSource)));
        final PageXResourceDomain pageXResourceDomain = mongoTemplate.findOne(findRefBySource, PageXResourceDomain.class, PAGE_XREF_COLLECTION_NAME);
        if(pageXResourceDomain==null) {
            Logger.info("Deleting image " + imgSource + " from db");
            final Query findResourceBySource = new Query(Criteria.where("_id").is(imgSource));
            mongoTemplate.remove(findResourceBySource, ImgResourceDomain.class, COLLECTION_NAME);
        }else{
            Logger.info("Can't delete image, there is linked page " + pageXResourceDomain.getPageUrl());
        }
    }

    @Override
    public void save(final ImgResource imgResource) throws IOException {
        Logger.info("Saving image " + imgResource.getSource() + "to db");
        try {
            final ImgResourceDomain imgResourceDomain = imgResourceImgResourceDomainConverter.convert(imgResource);
            mongoTemplate.save(imgResourceDomain, COLLECTION_NAME);
        }catch (DuplicateKeyException e){
            throw new IOException(e);
        }
    }

    @Override
    public void clear() {
        mongoTemplate.remove(new Query(), COLLECTION_NAME);
    }
}
