package daos.mongoImpl;

import daos.JsDao;
import domains.JsResourceDomain;
import domains.PageXResourceDomain;
import models.JsResource;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.convert.converter.Converter;
import org.springframework.dao.DuplicateKeyException;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.stereotype.Repository;
import play.Logger;

import java.io.IOException;
import java.util.List;

/**
 * Created by adippel on 29.10.2015.
 */
@Repository("jsDaoMongo")
public class JsDaoMongo implements JsDao {
    private static final String COLLECTION_NAME = "js";
    private static final String PAGE_XREF_COLLECTION_NAME = "pageScriptXRef";
    @Autowired
    Converter<JsResource, JsResourceDomain> jsResourceJsResourceDomainConverter;
    @Autowired
    Converter<JsResourceDomain, JsResource> jsResourceDomainJsResourceConverter;
    @Autowired
    MongoTemplate mongoTemplate;

    @Override
    public JsResource find(final String jsSource){
        Logger.trace("Searching script " + jsSource + " in db");
        final Query findResourceBySource  = new Query(Criteria.where("_id").is(jsSource));
        final List<JsResourceDomain> jsResourceDomains = mongoTemplate.find(findResourceBySource, JsResourceDomain.class, COLLECTION_NAME);
        if(jsResourceDomains == null || jsResourceDomains.isEmpty()){
            return null;
        }
        return jsResourceDomainJsResourceConverter.convert(jsResourceDomains.get(0));
    }

    @Override
    public void delete(final String jsSource){
        final Query findRefBySource = new Query((Criteria.where("source").is(jsSource)));
        PageXResourceDomain pageXResourceDomain = mongoTemplate.findOne(findRefBySource, PageXResourceDomain.class, PAGE_XREF_COLLECTION_NAME);
        if(pageXResourceDomain==null) {
            Logger.info("Deleting script " + jsSource + " from db");
            final Query findResourceBySource = new Query(Criteria.where("_id").is(jsSource));
            mongoTemplate.remove(findResourceBySource, JsResourceDomain.class, COLLECTION_NAME);
        }else{
            Logger.info("Cant delete script, there is linked page " + pageXResourceDomain.getPageUrl());
        }
    }

    @Override
    public void save(final JsResource jsResource) throws IOException {
        Logger.info("Saving image " + jsResource.getSource() + "to db");
        try {
            final JsResourceDomain jsResourceDomain = jsResourceJsResourceDomainConverter.convert(jsResource);
            mongoTemplate.save(jsResourceDomain, COLLECTION_NAME);
        }catch (DuplicateKeyException e){
            Logger.warn(e.getMessage());
            throw new IOException(e);
        }
    }

    @Override
    public void clear() {
        mongoTemplate.remove(new Query(), COLLECTION_NAME);
    }
}
