package daos.mongoImpl;

import daos.PageDao;
import domains.PageDomain;
import domains.PageDomainContent;
import domains.PageXResourceDomain;
import enums.LoadStatus;
import javassist.NotFoundException;
import models.FilterParameter;
import models.Page;
import models.PageIndexReport;
import models.exceptions.AlreadyExistsException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.convert.converter.Converter;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.data.mongodb.core.query.Update;
import org.springframework.stereotype.Repository;
import play.Logger;

import java.io.IOException;
import java.net.URL;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.Set;


/**
 * Created by adippel on 20.10.2015.
 */
@Repository("pageDaoMongo")
public class PageDaoMongo implements PageDao {

    private static final String COLLECTION_NAME = "page";
    private static final String PAGE_X_IMAGE_COLLECTION_NAME = "pageImageXRef";
    private static final String PAGE_X_SCRIPT_COLLECTION_NAME = "pageScriptXRef";
    private static final String PAGE_X_CSS_COLLECTION_NAME = "pageCssXRef";
    @Autowired
    MongoTemplate mongoTemplate;
    @Autowired
    Converter<Page, PageDomain> pageToPageDomain;
    @Autowired
    Converter<PageDomain, Page> pageDomainToPage;

    @Override
    public void insert(final Page page) throws AlreadyExistsException {
        try {
            Logger.info("Saving page " + page.getUrl() + "in db");
            final PageDomain pageDomain = pageToPageDomain.convert(page);
            mongoTemplate.insert(pageDomain, COLLECTION_NAME);
            Logger.info("Page " + page + " is saved in db");
        }catch (final org.springframework.dao.DuplicateKeyException e){
            Logger.warn("Page with url "+ page.getUrl() + " already exist");
            throw new AlreadyExistsException("Page with url "+ page.getUrl() + " already exist");
        }
    }

    @Override
    public void updateFullContent(final Page page) {
        Logger.trace("Updating full content " + page.getUrl() + "in db");
        final PageDomain pageDomain = pageToPageDomain.convert(page);
        final Query findPageByUrl = new Query(Criteria.where("_id").is(pageDomain.getUrl()));
        final Update update = new Update()
                .set("status", pageDomain.getStatus())
                .set("content", pageDomain.getContent())
                .set("subUrls", pageDomain.getSubUrls());
        mongoTemplate.updateFirst(findPageByUrl, update, PageDomain.class, COLLECTION_NAME);

        final PageDomainContent pageDomainContent = pageDomain.getContent();
        final Set<String> imgSources = pageDomainContent.getImages();
        createCrossRefs(pageDomain.getUrl(), imgSources, PAGE_X_IMAGE_COLLECTION_NAME);
        final Set<String> jsSources = pageDomainContent.getJsList();
        createCrossRefs(pageDomain.getUrl(), jsSources, PAGE_X_SCRIPT_COLLECTION_NAME);
        final Set<String> cssSources =  pageDomainContent.getCssList();
        createCrossRefs(pageDomain.getUrl(), cssSources, PAGE_X_CSS_COLLECTION_NAME);
    }

    private void createCrossRefs(final String pageUrl, final Set<String> sources, final String collectionName){
        for(final String source : sources){
            final PageXResourceDomain pageXResourceDomain = new PageXResourceDomain();
            pageXResourceDomain.setSource(source);
            pageXResourceDomain.setPageUrl(pageUrl);
            mongoTemplate.save(pageXResourceDomain, collectionName);
        }
    }

    @Override
    public void updateStatus(final URL url, final LoadStatus loadStatus) {
        Logger.trace("Updating url " + url + " status "+ loadStatus +" in db");
        final Query findPageByUrl = new Query(Criteria.where("_id").is(url));
        final Update update = new Update().set("status", loadStatus);
        mongoTemplate.updateFirst(findPageByUrl, update, PageDomain.class, COLLECTION_NAME);
    }

    @Override
    public void updateIndexReport(final Page page) {
        Logger.trace("Updating index report " + page.getUrl() + "in db");
        final PageDomain pageDomain = pageToPageDomain.convert(page);
        final Query findPageByUrl = new Query(Criteria.where("_id").is(pageDomain.getUrl()));
        final Update update = new Update()
                .set("indexReport", pageDomain.getIndexReport())
                .set("status", page.getStatus());
        mongoTemplate.updateFirst(findPageByUrl, update, PageDomain.class, COLLECTION_NAME);
    }

    @Override
    public LoadStatus getStatus(final URL url) throws NotFoundException {
        Logger.trace("Getting url " + url + " status from db");
        final Query findLoadStatusByUrl = new Query(Criteria.where("_id").is(url));
        findLoadStatusByUrl.fields().include("status");
        final PageDomain pageDomain = mongoTemplate.findOne(findLoadStatusByUrl, PageDomain.class, COLLECTION_NAME);
        if(pageDomain ==null) {
            throw new NotFoundException("Page with url " + url +" not found");
        }
        return pageDomain.getStatus();
    }

    @Override
    public Page getPageContentAndSubUrls(final URL url) {
        Logger.trace("Getting page content by url " + url + " from db");
        final Query findPageContentByUrl = new Query(Criteria.where("_id").is(url));
        findPageContentByUrl.fields().include("content");
        findPageContentByUrl.fields().include("subUrls");
        final PageDomain pageDomain = mongoTemplate.findOne(findPageContentByUrl, PageDomain.class, COLLECTION_NAME);
        if(pageDomain==null){
            return null;
        }
        final Page page = pageDomainToPage.convert(pageDomain);
        return page;
    }

    @Override
    public PageIndexReport getIndexReport(final URL url) throws NotFoundException {
        Logger.trace("Getting index report about " + url + "  from db");
        final Query findIndexReportByUrl = new Query(Criteria.where("_id").is(url));
        findIndexReportByUrl.fields().include("indexReport");
        final PageDomain pageDomain = mongoTemplate.findOne(findIndexReportByUrl, PageDomain.class, COLLECTION_NAME);
        if(pageDomain ==null) {
            throw new NotFoundException("Index report for url "+url+" not found");
        }
        final Page page = pageDomainToPage.convert(pageDomain);
        return page.getIndexReport();
    }

    @Override
    public Page findPageAndRemove(final URL url) {
        Logger.info("Deleting url " + url + " from db");
        final Query findPageByUrl = new Query(Criteria.where("_id").is(url));
        findPageByUrl.fields().include("status");
        findPageByUrl.fields().include("subUrls");
        findPageByUrl.fields().include("content");
        findPageByUrl.fields().include("content.images");
        findPageByUrl.fields().include("content.jsList");
        findPageByUrl.fields().include("content.cssList");
        final PageDomain pageDomain = mongoTemplate.findAndRemove(findPageByUrl, PageDomain.class, COLLECTION_NAME);
        if(pageDomain == null){
            return null;
        }
        removeCrossRefs(pageDomain.getUrl(), PAGE_X_CSS_COLLECTION_NAME);
        removeCrossRefs(pageDomain.getUrl(), PAGE_X_SCRIPT_COLLECTION_NAME);
        removeCrossRefs(pageDomain.getUrl(), PAGE_X_IMAGE_COLLECTION_NAME);

        return pageDomainToPage.convert(pageDomain);
    }

    private void removeCrossRefs(final String pageUrl, final String collectionName){
        final Query findByPageUrl = new Query(Criteria.where("pageUrl").is(pageUrl));
        mongoTemplate.remove(findByPageUrl, collectionName);
    }

    @Override
    public long  getPageCountByStatus(final LoadStatus loadStatus) {
        final Query findPageByStatus = new Query(Criteria.where("status").is(loadStatus));
        final long count = mongoTemplate.count(findPageByStatus, PageDomain.class, COLLECTION_NAME);
        return count;
    }

    @Override
    public List<Page> getPartialPagesWithTextContentByStatus(final LoadStatus loadStatus, final int skip, final int limit) {
        final Query findPageByStatus = new Query(Criteria.where("status").is(loadStatus));
        findPageByStatus.skip(skip);
        findPageByStatus.limit(limit);
        findPageByStatus.fields().include("content.titleContent");
        findPageByStatus.fields().include("content.bodyContent");
        final List<PageDomain> pageDomains = mongoTemplate.find(findPageByStatus, PageDomain.class, COLLECTION_NAME);
        final List<Page> pages = new LinkedList<>();
        for(final PageDomain pageDomain : pageDomains){
            pages.add(pageDomainToPage.convert(pageDomain));
        }
        return pages;
    }

    @Override
    public List<Page> getFilteredPageList(final FilterParameter filterParameter) {
        final Criteria finalCriteria = getCriteria(filterParameter);
        final Query findPagesByFilteredCriteria = new Query();
        findPagesByFilteredCriteria.addCriteria(finalCriteria);
        findPagesByFilteredCriteria.fields().include("_id");
        findPagesByFilteredCriteria.fields().include("status");
        final List<PageDomain> filteredPageDomains = mongoTemplate.find(findPagesByFilteredCriteria,PageDomain.class, COLLECTION_NAME);
        final List<Page> pages = new ArrayList<>();
        for(final PageDomain pageDomain : filteredPageDomains){
            pages.add(pageDomainToPage.convert(pageDomain));
        }
        return pages;
    }

    @Override
    public long getPageCount() throws IOException {
        final long count = mongoTemplate.count(new Query(), PageDomain.class, COLLECTION_NAME);
        return count;
    }

    @Override
    public void clear() throws IOException {
        mongoTemplate.remove(new Query(), COLLECTION_NAME);
    }

    private Criteria getCriteria(final FilterParameter filterParameter) {
        final List<Criteria> criterias = new LinkedList<>();

        if(filterParameter.getUrlParameter()!=null){
            final Criteria urlCriteria = Criteria.where("_id").regex(filterParameter.getUrlParameter(), "i");
            criterias.add(urlCriteria);
        }
        if(filterParameter.getTitleParameter()!=null){
            final Criteria titleCriteria = Criteria.where("content.titleContent").regex(filterParameter.getTitleParameter(), "i");
            criterias.add(titleCriteria);
        }
        if(filterParameter.getBodyParameter()!=null){
            final Criteria bodyCriteria = Criteria.where("content.bodyContent").regex(filterParameter.getBodyParameter(), "i");
            criterias.add(bodyCriteria);
        }
        if(filterParameter.getDateParameter()!=null){
            final Criteria dateCriteria = Criteria.where("content.lastUpdate").gt(filterParameter.getDateParameter());
            criterias.add(dateCriteria);
        }
        Logger.debug("Criterias count " + criterias.size());
        final Criteria finalCriteria = new Criteria().andOperator(criterias.toArray(new Criteria[criterias.size()]));
        Logger.debug(finalCriteria.getCriteriaObject().toString());
        return finalCriteria;
    }
}
