package daos.mongoImpl;

import daos.CssDao;
import domains.CssResourceDomain;
import domains.PageXResourceDomain;
import models.CssResource;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.convert.converter.Converter;
import org.springframework.dao.DuplicateKeyException;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.stereotype.Repository;
import play.Logger;

import java.io.IOException;
import java.util.List;

/**
 * Created by adippel on 29.10.2015.
 */
@Repository("cssDaoMongo")
public class CssDaoMongo implements CssDao {
    private static final String COLLECTION_NAME = "css";
    private static final String PAGE_XREF_COLLECTION_NAME = "pageCssXRef";
    @Autowired
    Converter<CssResource, CssResourceDomain> cssResourceCssResourceDomainConverter;
    @Autowired
    Converter<CssResourceDomain, CssResource> cssResourceDomainCssResourceConverter;
    @Autowired
    MongoTemplate mongoTemplate;

    @Override
    public CssResource find(final String cssSource)  {
        Logger.trace("Searching css " + cssSource + " in db");
        final Query findResourceBySource  = new Query(Criteria.where("_id").is(cssSource));
        final List<CssResourceDomain> cssResourceDomains = mongoTemplate.find(findResourceBySource, CssResourceDomain.class, COLLECTION_NAME);
        if (cssResourceDomains == null || cssResourceDomains.isEmpty()) {
            return null;
        }
        return cssResourceDomainCssResourceConverter.convert(cssResourceDomains.get(0));
    }

    @Override
    public void delete(final String cssSource) {
        final Query findRefBySource = new Query((Criteria.where("source").is(cssSource)));
        final PageXResourceDomain pageXResourceDomain = mongoTemplate.findOne(findRefBySource, PageXResourceDomain.class, PAGE_XREF_COLLECTION_NAME);
        if(pageXResourceDomain == null) {
            Logger.info("Deleting css " + cssSource + " from db");
            final Query findResourceBySource = new Query(Criteria.where("_id").is(cssSource));
            mongoTemplate.remove(findResourceBySource, CssResourceDomain.class, COLLECTION_NAME);
        }else{
            Logger.info("Cant delete script, there is linked page " + pageXResourceDomain.getPageUrl());
        }
    }

    @Override
    public void save(final CssResource cssResource) throws IOException {
        Logger.info("Saving image " + cssResource.getSource() + "to db");
        final CssResourceDomain cssResourceDomain = cssResourceCssResourceDomainConverter.convert(cssResource);
        try {
            mongoTemplate.save(cssResourceDomain, COLLECTION_NAME);
        }catch (DuplicateKeyException e){
            throw new IOException(e);
        }
    }

    @Override
    public void clear() {
        mongoTemplate.remove(new Query(), COLLECTION_NAME);
    }
}
