package daos.mongoImpl;

import daos.IndexDao;
import domains.IndexDomain;
import models.Index;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.convert.converter.Converter;
import org.springframework.dao.DuplicateKeyException;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.stereotype.Repository;
import play.Logger;

import java.io.IOException;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

/**
 * Created by adippel on 23.10.2015.
 */
@Repository("indexDaoMongo")
public class IndexDaoMongo implements IndexDao {


    private static final String COLLECTION_NAME = "index";
    @Autowired
    MongoTemplate mongoTemplate;
    @Autowired
    private Converter<Index, IndexDomain> indexIndexDomainConverter;
    @Autowired
    private Converter<IndexDomain, Index> indexDomainIndexConverter;

    @Override
    public void save(final Index index) throws IOException {
        Logger.info("Inserting index " + index + "in db");
        try {
            final IndexDomain indexDomain = indexIndexDomainConverter.convert(index);
            mongoTemplate.insert(indexDomain, COLLECTION_NAME);
        }catch (DuplicateKeyException e){
            Logger.warn(e.getMessage());
            throw new IOException(e);
        }
    }

    @Override
    public void delete(final Index index){
        Logger.info("Deleting index " + index + " from db");
        final IndexDomain indexDomain = indexIndexDomainConverter.convert(index);
        final Query findIndex = new Query(Criteria.where("_id").is(indexDomain.getIndex()));
        mongoTemplate.remove(findIndex, IndexDomain.class, COLLECTION_NAME);
    }

    @Override
    public Set<Index> findAll() {
        final Set<Index> indexes = new HashSet<>();
        final List<IndexDomain> indexDomains = mongoTemplate.findAll(IndexDomain.class, COLLECTION_NAME);
        for(final IndexDomain indexDomain : indexDomains){
            indexes.add(indexDomainIndexConverter.convert(indexDomain));
        }
        return indexes;
    }

    @Override
    public void clear() {
        mongoTemplate.remove(new Query(), COLLECTION_NAME);
    }
}
