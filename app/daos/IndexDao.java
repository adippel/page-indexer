package daos;

import models.Index;
import org.springframework.stereotype.Repository;

import java.io.IOException;
import java.util.Set;

/**
 * Created by adippel on 23.10.2015.
 */
@Repository
public interface IndexDao {

    void save(Index index) throws IOException;
    void delete(Index index) throws IOException;
    Set<Index> findAll() throws IOException;

    //for test purposes
    void clear() throws IOException;





}
