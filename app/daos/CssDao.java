package daos;

import models.CssResource;

import java.io.IOException;

/**
 * Created by adippel on 29.10.2015.
 */
public interface CssDao {
    CssResource find(String cssSource) throws IOException;
    void delete(String cssSource) throws IOException;
    void save(CssResource cssResource) throws IOException;

    //for test purposes
    void clear() throws IOException;



}
