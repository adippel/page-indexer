package daos;

import enums.LoadStatus;
import javassist.NotFoundException;
import models.FilterParameter;
import models.Page;
import models.PageIndexReport;
import models.exceptions.AlreadyExistsException;

import java.io.IOException;
import java.net.URL;
import java.util.List;

/**
 * Created by adippel on 20.10.2015.
 */
public interface PageDao {

    void insert(Page page) throws AlreadyExistsException, IOException;
    void updateFullContent(Page page) throws IOException;
    void updateStatus(URL url, LoadStatus loadStatus) throws IOException;
    void updateIndexReport(Page page) throws IOException;
    LoadStatus getStatus(URL url) throws NotFoundException, IOException;
    PageIndexReport getIndexReport(URL url) throws NotFoundException, IOException;
    Page getPageContentAndSubUrls(URL url) throws IOException;
    Page findPageAndRemove(URL url) throws IOException;
    long getPageCountByStatus(LoadStatus loadStatus) throws IOException;
    List<Page> getPartialPagesWithTextContentByStatus(LoadStatus loadStatus, int skip, int limit) throws IOException;
    List<Page> getFilteredPageList(FilterParameter filterParameter) throws IOException;

    //for test purposes
    long getPageCount() throws IOException;
    void clear() throws IOException;


}
