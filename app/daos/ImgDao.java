package daos;

import models.ImgResource;

import java.io.IOException;

/**
 * Created by adippel on 29.10.2015.
 */
public interface ImgDao {
    ImgResource find(String imgSource) throws IOException;
    void delete(String imgSource) throws IOException;
    void save(ImgResource imgResource) throws IOException;

    //for test purposes
    void clear() throws IOException;


}
