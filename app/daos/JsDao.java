package daos;

import models.JsResource;

import java.io.IOException;

/**
 * Created by adippel on 29.10.2015.
 */
public interface JsDao {
    JsResource find(String jsSource) throws IOException;
    void delete(String jsSource) throws IOException;
    void save(JsResource jsResource) throws IOException;

    //for test purposes
    void clear() throws IOException;

}
