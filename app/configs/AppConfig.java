package configs;

import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.ImportResource;

@Configuration
@ComponentScan({"controllers", "services", "daos", "converters", "domains", "utils", "mappers"})
@ImportResource({"classpath:props.xml", "classpath:jdbc_config.xml"})
public class AppConfig {

}