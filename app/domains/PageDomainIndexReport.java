package domains;

import org.springframework.data.mongodb.core.mapping.Document;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by adippel on 23.10.2015.
 */
@Document
public class PageDomainIndexReport {

    private Map<String, Integer> indexMap = new HashMap<>();
    public PageDomainIndexReport() {
    }

    public Map<String, Integer> getIndexMap() {
        return indexMap;
    }

    public void setIndexMap(final Map<String, Integer> indexMap) {
        this.indexMap = indexMap;
    }
}
