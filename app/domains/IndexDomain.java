package domains;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

/**
 * Created by adippel on 23.10.2015.
 */
@Document
public class IndexDomain {
    @Id
    private String index;

    public IndexDomain() {
    }

    public IndexDomain(String index) {
        this.index = index;
    }

    public String getIndex() {
        return index;
    }

    public void setIndex(String index) {
        this.index = index;
    }
}
