package domains;

import enums.LoadStatus;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import java.net.URL;
import java.util.Set;

/**
 * Created by adippel on 20.10.2015.
 */
@Document
public class PageDomain {
    @Id
    private String url;
    private LoadStatus status;
    private PageDomainContent content;
    private PageDomainIndexReport indexReport;
    private Set<URL> subUrls;

    public PageDomain() {
    }

    public PageDomain(final String url, final LoadStatus status) {
        this.url = url;
        this.status = status;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(final String url) {
        this.url = url;
    }

    public LoadStatus getStatus() {
        return status;
    }

    public void setStatus(final LoadStatus status) {
        this.status = status;
    }

    public PageDomainContent getContent() {
        return content;
    }

    public void setContent(final PageDomainContent content) {
        this.content = content;
    }

    public Set<URL> getSubUrls() {
        return subUrls;
    }

    public void setSubUrls(final Set<URL> subUrls) {
        this.subUrls = subUrls;
    }

    public PageDomainIndexReport getIndexReport() {
        return indexReport;
    }

    public void setIndexReport(final PageDomainIndexReport indexReport) {
        this.indexReport = indexReport;
    }

    @Override
    public String toString() {
        return "PageDomain{" +
                "url='" + url + '\'' +
                ", status=" + status +
                ", content=" + content +
                ", indexReport=" + indexReport +
                ", subUrls=" + subUrls +
                '}';
    }
}
