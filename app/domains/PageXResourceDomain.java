package domains;

import org.springframework.data.mongodb.core.mapping.Document;

/**
 * Created by adippel on 29.10.2015.
 */
@Document
public class PageXResourceDomain {
    private String pageUrl;
    private String source;

    public String getPageUrl() {
        return pageUrl;
    }

    public void setPageUrl(final String pageUrl) {
        this.pageUrl = pageUrl;
    }

    public String getSource() {
        return source;
    }

    public void setSource(final String source) {
        this.source = source;
    }
}
