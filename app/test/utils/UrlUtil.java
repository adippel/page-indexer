package test.utils;

import models.IteratedUrl;

import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by adippel on 12/2/15.
 */
public class UrlUtil {
    private static final String URL_TEMPLATE = "https://www.test123.com/";

    public static List<URL> generateUrlList(int size) {
        List<URL> urls = new ArrayList<>(size);
        for(int i = 0; i < size; i++){
            try {
                URL url = new URL(URL_TEMPLATE + i);
                urls.add(url);
            } catch (MalformedURLException e) {
                e.printStackTrace();
            }
        }
        return urls;
    }

    public static List<IteratedUrl> generateIteratedUrlList(int size) {
        List<IteratedUrl> iteratedUrls = new ArrayList<>(size);
        for(int i = 0; i < size; i++){
            try {
                URL url = new URL(URL_TEMPLATE + i);
                IteratedUrl iteratedUrl = new IteratedUrl(url, 0);
                iteratedUrls.add(iteratedUrl);
            } catch (MalformedURLException e) {
                e.printStackTrace();
            }
        }
        return iteratedUrls;
    }
}
