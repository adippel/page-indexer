package test;


import configs.AppConfig;
import configs.MongoConfig;
import daos.IndexDao;
import daos.PageDao;
import enums.LoadStatus;
import javassist.NotFoundException;
import models.FilterParameter;
import models.Index;
import models.Page;
import models.PageIndexReport;
import models.exceptions.AlreadyExistsException;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.support.AnnotationConfigContextLoader;
import services.IndexService;
import services.PagePipeline;
import services.PageService;

import java.io.IOException;
import java.net.URL;
import java.util.List;
import java.util.Map;

/**
 * Created by adippel on 11/27/15.
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = {AppConfig.class, MongoConfig.class}, loader = AnnotationConfigContextLoader.class)
public class PageServiceImplTest {

    @Autowired
    PageService pageService;
    @Autowired
    IndexService indexService;
    @Autowired
    @Qualifier("primaryPageDao")
    PageDao pageDao;
    @Autowired
    @Qualifier("primaryIndexDao")
    IndexDao indexDao;
    @Autowired
    private PagePipeline pagePipeline;

    @Before
    public void setup() throws IOException {
        pagePipeline.initListeners();
        pageService.clearAllData();
        indexService.clear();

    }

    @After
    public void clear() throws IOException {
        pageService.clearAllData();
        indexService.clear();
    }

    @Test
    public void positiveAddPage() throws IOException, AlreadyExistsException, InterruptedException {
        URL url = new URL("https://www.playframework.com/");
        pageService.addPage(url);
        Thread.sleep(10000);
        long count = pageDao.getPageCountByStatus(LoadStatus.READY);
        Assert.assertEquals("Must be 7 ready db rows ", 7, count);
    }

    @Test
    public void positiveDeletePage() throws IOException, AlreadyExistsException, InterruptedException, NotFoundException {
        URL url = new URL("https://www.playframework.com/");
        positiveAddPage();
        pageService.deletePage(url);
        pageDao.getPageCountByStatus(LoadStatus.READY);
        long count = pageDao.getPageCount();
        Assert.assertEquals("Must be 0 db rows", 0, count);
    }

    @Test
    public void positiveGetStatus() throws IOException, AlreadyExistsException, InterruptedException, NotFoundException {
        URL url = new URL("https://www.playframework.com/");
        positiveAddPage();
        LoadStatus loadStatus = pageService.getStatus(url);
        Assert.assertEquals("Page status must be READY", loadStatus, LoadStatus.READY);
    }

    @Test
    public void positiveGetFilteredPages() throws IOException, AlreadyExistsException, InterruptedException, NotFoundException {
        URL url = new URL("https://www.playframework.com/");
        positiveAddPage();
        FilterParameter filterParameter = new FilterParameter();
        filterParameter.setTitleParameter("downloads");
        List<Page> filteredPages = pageService.getFilteredPages(filterParameter);
        Assert.assertEquals("2 pages must be filtered with downloads title " ,2, filteredPages.size());
    }

    @Test
    public void positiveGetEmptyFilteredPages() throws NotFoundException, IOException, AlreadyExistsException, InterruptedException {
        URL url = new URL("https://www.playframework.com/");
        positiveAddPage();
        FilterParameter filterParameter = new FilterParameter();
        filterParameter.setTitleParameter("Not existing title");
        List<Page> filteredPages = pageService.getFilteredPages(filterParameter);
        Assert.assertEquals("0 pages must be filtered with downloads title ",0, filteredPages.size());
    }

    @Test
    public void positiveGetIndexReport() throws IOException, AlreadyExistsException, InterruptedException, NotFoundException {
        URL url = new URL("https://www.playframework.com/");
        indexDao.save(new Index("play"));
        positiveAddPage();
        PageIndexReport pageIndexReport = pageDao.getIndexReport(url);
        Map<String, Integer> indexes = pageIndexReport.getIndexMap();
        Assert.assertEquals("Count must be 17 for index play ", new Integer(17), indexes.get("play"));
    }

    @Test(expected = NotFoundException.class)
    public void negativeDeleteDownloadingPage() throws IOException, AlreadyExistsException, InterruptedException, NotFoundException {
        URL url = new URL("https://www.playframework.com/");
        pageService.addPage(url);
        pageService.deletePage(url);
    }

    @Test(expected = AlreadyExistsException.class)
    public void negativeAddExistingPage() throws IOException, AlreadyExistsException, InterruptedException, NotFoundException {
        URL url = new URL("https://www.playframework.com/");
        pageService.addPage(url);
        pageService.addPage(url);
    }

    @Test(expected = NotFoundException.class)
    public void negativeGetNotExistingPageStatus() throws NotFoundException, IOException {
        URL url = new URL("https://www.playframework.com/");
        pageService.getStatus(url);
    }

    @Test(expected = NotFoundException.class)
    public void negativeDeleteNotExistingPage() throws NotFoundException, IOException {
        URL url = new URL("https://www.playframework.com/");
        pageService.deletePage(url);
    }

    @Test(expected = NotFoundException.class)
    public void negativeGetNotExistingPageIndexReport() throws NotFoundException, IOException {
        URL url = new URL("https://www.playframework.com/");
        pageService.getPageIndexReport(url);
    }













}
