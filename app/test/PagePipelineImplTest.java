package test;

import configs.AppConfig;
import configs.MongoConfig;
import models.IteratedUrl;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.support.AnnotationConfigContextLoader;
import services.PagePipeline;
import services.PageService;
import test.utils.UrlUtil;

import java.io.IOException;
import java.net.URL;
import java.util.List;


/**
 * Created by adippel on 12/2/15.
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = {AppConfig.class, MongoConfig.class}, loader = AnnotationConfigContextLoader.class)
public class PagePipelineImplTest {

    @Autowired
    PageService pageService;

    @Autowired
    private PagePipeline pagePipeline;

    @Before
    public void setup() throws IOException {
        pagePipeline.initListeners();
        pageService.clearAllData();

    }

    @After
    public void clear() throws IOException {
    }

    @Test
    public void positiveAddToAddingQueue() throws InterruptedException {
        List<IteratedUrl> iteratedUrls = UrlUtil.generateIteratedUrlList(1000000);
        for(IteratedUrl iteratedUrl : iteratedUrls){
            pagePipeline.addToAddingQueue(iteratedUrl);
        }
        Thread.sleep(4000);
        int size = pagePipeline.getAddingQueueSize();
        Assert.assertEquals("Url adding Queue should be empty", 0, size);
    }

    @Test
    public void positiveAddToIndexingQueue() throws InterruptedException {
        List<URL> urls = UrlUtil.generateUrlList(1000000);
        for (URL url : urls) {
            pagePipeline.addToIndexingQueue(url);
        }
        Thread.sleep(2000);
        int size = pagePipeline.getAddingQueueSize();
        Assert.assertEquals("Url indexing Queue should be empty", 0, size);
    }

    @Test
    public void positiveLockAddQueue() throws InterruptedException {
        pagePipeline.lockAddQueue();
        List<IteratedUrl> iteratedUrls = UrlUtil.generateIteratedUrlList(100);
        for(IteratedUrl iteratedUrl : iteratedUrls){
            pagePipeline.addToAddingQueue(iteratedUrl);
        }
        int size = pagePipeline.getAddingQueueSize();
        Thread.sleep(1000);
        Assert.assertEquals("Url indexing Queue should contain 100 urls when locked", 99, size);
        pagePipeline.unlockAddQueue();
        Thread.sleep(1000);
        size = pagePipeline.getAddingQueueSize();
        Assert.assertEquals("Url indexing Queue should contain 0 urls after unlocked", 0, size);
    }
}
