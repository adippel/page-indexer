package converters.domainconverters.resourceconverters;

import domains.ImgResourceDomain;
import models.ImgResource;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

/**
 * Created by adippel on 29.10.2015.
 */
@Component
public class ImgResourceToImgResourceDomainConverter implements Converter<ImgResource, ImgResourceDomain> {
    @Override
    public ImgResourceDomain convert(final ImgResource imgResource) {
        final ImgResourceDomain imgResourceDomain = new ImgResourceDomain();
        imgResourceDomain.setContent(imgResource.getByteContent());
        imgResourceDomain.setSource(imgResource.getSource());
        return imgResourceDomain;
    }
}
