package converters.domainconverters.resourceconverters;

import domains.CssResourceDomain;
import models.CssResource;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

/**
 * Created by adippel on 29.10.2015.
 */
@Component
public class CssResourceToCssResourceDomainConverter implements Converter<CssResource, CssResourceDomain> {
    @Override
    public CssResourceDomain convert(final CssResource cssResource) {
        final CssResourceDomain cssResourceDomain = new CssResourceDomain();
        cssResourceDomain.setContent(cssResource.getContent());
        cssResourceDomain.setSource(cssResource.getSource());
        return cssResourceDomain;
    }
}
