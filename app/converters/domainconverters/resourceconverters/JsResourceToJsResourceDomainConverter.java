package converters.domainconverters.resourceconverters;

import domains.JsResourceDomain;
import models.JsResource;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

/**
 * Created by adippel on 29.10.2015.
 */
@Component
public class JsResourceToJsResourceDomainConverter implements Converter<JsResource, JsResourceDomain> {
    @Override
    public JsResourceDomain convert(final JsResource jsResource) {
        final JsResourceDomain jsResourceDomain = new JsResourceDomain();
        jsResourceDomain.setContent(jsResource.getContent());
        jsResourceDomain.setSource(jsResource.getSource());
        return jsResourceDomain;
    }
}
