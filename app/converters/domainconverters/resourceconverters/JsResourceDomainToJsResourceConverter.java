package converters.domainconverters.resourceconverters;

import domains.JsResourceDomain;
import models.JsResource;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

/**
 * Created by adippel on 29.10.2015.
 */
@Component
public class JsResourceDomainToJsResourceConverter implements Converter<JsResourceDomain, JsResource> {
    @Override
    public JsResource convert(final JsResourceDomain jsResourceDomain) {
        final JsResource jsResource = new JsResource();
        jsResource.setContent(jsResourceDomain.getContent());
        jsResource.setSource(jsResourceDomain.getSource());
        return jsResource;
    }
}
