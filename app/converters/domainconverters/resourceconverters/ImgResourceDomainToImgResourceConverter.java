package converters.domainconverters.resourceconverters;

import domains.ImgResourceDomain;
import models.ImgResource;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

/**
 * Created by adippel on 29.10.2015.
 */
@Component
public class ImgResourceDomainToImgResourceConverter implements Converter<ImgResourceDomain, ImgResource> {
    @Override
    public ImgResource convert(final ImgResourceDomain imgResourceDomain) {
        final ImgResource imgResource = new ImgResource();
        imgResource.setByteContent(imgResourceDomain.getContent());
        imgResource.setSource(imgResourceDomain.getSource());
        return imgResource;

    }
}
