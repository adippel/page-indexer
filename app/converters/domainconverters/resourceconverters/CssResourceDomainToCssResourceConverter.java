package converters.domainconverters.resourceconverters;

import domains.CssResourceDomain;
import models.CssResource;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

/**
 * Created by adippel on 29.10.2015.
 */
@Component
public class CssResourceDomainToCssResourceConverter implements Converter<CssResourceDomain, CssResource> {
    @Override
    public CssResource convert(final CssResourceDomain cssResourceDomain) {
        final CssResource cssResource = new CssResource();
        cssResource.setContent(cssResourceDomain.getContent());
        cssResource.setSource(cssResourceDomain.getSource());
        return cssResource;
    }
}
