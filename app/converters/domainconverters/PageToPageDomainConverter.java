package converters.domainconverters;

import domains.PageDomain;
import domains.PageDomainContent;
import domains.PageDomainIndexReport;
import models.Page;
import models.PageContent;
import models.PageIndexReport;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

/**
 * Created by adippel on 20.10.2015.
 */
@Component
public class PageToPageDomainConverter implements Converter<Page, PageDomain> {
    @Override
    public PageDomain convert(final Page page) {
        final PageDomain pageDomain = new PageDomain();
        pageDomain.setUrl(page.getUrl().toString());
        pageDomain.setStatus(page.getStatus());
        pageDomain.setSubUrls(page.getSubUrls());
        final PageContent pageContent =  page.getContent();

        if(pageContent == null){
            pageDomain.setContent(null);
        }else {
            final PageDomainContent pageDomainContent = new PageDomainContent();
            pageDomainContent.setBodyContent(pageContent.getBodyContent());
            pageDomainContent.setTitleContent(pageContent.getTitleContent());
            pageDomainContent.setCssList(pageContent.getCssList());
            pageDomainContent.setImages(pageContent.getImages());
            pageDomainContent.setHtml(pageContent.getHtml());
            pageDomainContent.setLastUpdate(pageContent.getLastUpdate());
            pageDomainContent.setJsList(pageContent.getJsList());

            pageDomain.setContent(pageDomainContent);
        }

        final PageIndexReport pageIndexReport = page.getIndexReport();
        if(pageIndexReport == null){
            pageDomain.setIndexReport(null);
        }else{
            final PageDomainIndexReport pageDomainIndexReport = new PageDomainIndexReport();
            pageDomainIndexReport.setIndexMap(pageIndexReport.getIndexMap());

            pageDomain.setIndexReport(pageDomainIndexReport);
        }
        return pageDomain;
    }
}
