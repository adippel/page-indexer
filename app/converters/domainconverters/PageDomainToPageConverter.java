package converters.domainconverters;

import domains.PageDomain;
import domains.PageDomainContent;
import domains.PageDomainIndexReport;
import models.Page;
import models.PageContent;
import models.PageIndexReport;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;
import play.Logger;

import java.net.MalformedURLException;
import java.net.URL;

/**
 * Created by adippel on 20.10.2015.
 */
@Component
public class PageDomainToPageConverter implements Converter<PageDomain, Page> {

    @Override
    public Page convert(final PageDomain pageDomain) {
        final Page page= new Page();
        try {
            page.setUrl(new URL(pageDomain.getUrl()));
        } catch (MalformedURLException e) {
            Logger.warn("Malformed url " + pageDomain.getUrl());
        }
        page.setStatus(pageDomain.getStatus());
        page.setSubUrls(pageDomain.getSubUrls());
        final PageDomainContent pageDomainContent =  pageDomain.getContent();

        if(pageDomainContent == null){
            page.setContent(null);
        }else {
            final PageContent pageContent = new PageContent();
            pageContent.setBodyContent(pageDomainContent.getBodyContent());
            pageContent.setTitleContent(pageDomainContent.getTitleContent());
            pageContent.setCssList(pageDomainContent.getCssList());
            pageContent.setImages(pageDomainContent.getImages());
            pageContent.setHtml(pageDomainContent.getHtml());
            pageContent.setLastUpdate(pageDomainContent.getLastUpdate());
            pageContent.setJsList(pageDomainContent.getJsList());

            page.setContent(pageContent);
        }

        final PageDomainIndexReport pageDomainIndexReport = pageDomain.getIndexReport();
        if(pageDomainIndexReport == null){
            page.setIndexReport(null);
        }else{
            final PageIndexReport pageIndexReport = new PageIndexReport();
            pageIndexReport.setIndexMap(pageDomainIndexReport.getIndexMap());

            page.setIndexReport(pageIndexReport);
        }
        return page;
    }
}
