package converters.domainconverters;

import domains.IndexDomain;
import models.Index;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

/**
 * Created by adippel on 23.10.2015.
 */
@Component
public class IndexToIndexDomainConverter implements Converter<Index, IndexDomain> {
    @Override
    public IndexDomain convert(final Index index) {
        return new IndexDomain(index.getIndex());
    }
}
