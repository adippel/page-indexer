package converters.domainconverters;

import domains.IndexDomain;
import models.Index;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

/**
 * Created by adippel on 23.10.2015.
 */
@Component
public class IndexDomainToIndexConverter implements Converter<IndexDomain, Index> {
    @Override
    public Index convert(final IndexDomain indexDomain) {
        return new Index(indexDomain.getIndex());
    }
}
