import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import play.Application;
import play.GlobalSettings;
import services.impl.PagePipelineImpl;

public class Global extends GlobalSettings {

    private ApplicationContext ctx;

    @Override
    public void onStart(final Application app) {
        ctx = new AnnotationConfigApplicationContext("configs");
        final PagePipelineImpl pagePipeline = (PagePipelineImpl) ctx.getBean("pagePipeline");
        pagePipeline.initListeners();
    }

    @Override
    public <A> A getControllerInstance(final Class<A> clazz) {
        return ctx.getBean(clazz);
    }

    @Override
    public void onStop(final Application app){
        final PagePipelineImpl pagePipeline = (PagePipelineImpl) ctx.getBean("pagePipeline");
        pagePipeline.shutDownListeners();

    }
}