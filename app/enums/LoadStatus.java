package enums;

/**
 * Created by adippel on 20.10.2015.
 */
public enum LoadStatus {DOWNLOADING, INDEXING, READY}

