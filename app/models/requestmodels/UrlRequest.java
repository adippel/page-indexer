package models.requestmodels;

import java.net.URL;

/**
 * Created by adippel on 22.10.2015.
 */
public class UrlRequest {
    private URL url;

    public URL getUrl() {
        return url;
    }

    public void setUrl(URL url) {
        this.url = url;
    }
}
