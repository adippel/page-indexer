package models;

import java.net.URL;

/**
 * Created by adippel on 21.10.2015.
 */
public class IteratedUrl {

    private URL url;
    private int iterationRound;

    public IteratedUrl() {
    }

    public IteratedUrl(final URL url, final int iterationRound) {
        this.url = url;
        this.iterationRound = iterationRound;
    }

    public URL getUrl() {
        return url;
    }

    public void setUrl(final URL url) {
        this.url = url;
    }

    public int getIterationRound() {
        return iterationRound;
    }

    public void setIterationRound(final int iterationRound) {
        this.iterationRound = iterationRound;
    }

    @Override
    public String toString() {
        return "IteratedUrl{" +
                "url=" + url +
                ", iterationRound=" + iterationRound +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        IteratedUrl that = (IteratedUrl) o;

        return url.equals(that.url);

    }

    @Override
    public int hashCode() {
        return url.hashCode();
    }
}
