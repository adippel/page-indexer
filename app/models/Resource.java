package models;

/**
 * Created by adippel on 28.10.2015.
 */
public class Resource {
    private String source;

    public String getSource() {
        return source;
    }

    public void setSource(final String source) {
        this.source = source;
    }

    @Override
    public boolean equals(final Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        final Resource resource = (Resource) o;

        return !(source != null ? !source.equals(resource.source) : resource.source != null);

    }

    @Override
    public int hashCode() {
        return source != null ? source.hashCode() : 0;
    }
}
