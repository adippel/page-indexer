package models.exceptions;

/**
 * Created by adippel on 21.10.2015.
 */
public class AlreadyExistsException extends Exception {
   public AlreadyExistsException(String message){
        super(message);
   }
}
