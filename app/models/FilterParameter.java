package models;

import java.util.Date;

/**
 * Created by adippel on 26.10.2015.
 */
public class FilterParameter {
    private String urlParameter;
    private String titleParameter;
    private String bodyParameter;
    private Date dateParameter;

    public String getUrlParameter() {
        return urlParameter;
    }

    public void setUrlParameter(final String urlParameter) {
        this.urlParameter = urlParameter;
    }

    public String getTitleParameter() {
        return titleParameter;
    }

    public void setTitleParameter(final String titleParameter) {
        this.titleParameter = titleParameter;
    }

    public String getBodyParameter() {
        return bodyParameter;
    }

    public void setBodyParameter(final String bodyParameter) {
        this.bodyParameter = bodyParameter;
    }

    public Date getDateParameter() {
        return dateParameter;
    }

    public void setDateParameter(final Date dateParameter) {
        this.dateParameter = dateParameter;
    }

    public boolean isEmpty(){
        return ((urlParameter==null) && (titleParameter ==null) && (bodyParameter==null) && (dateParameter==null));
    }

    @Override
    public String toString() {
        return "FilterParameter{" +
                "urlParameter='" + urlParameter + '\'' +
                ", titleParameter='" + titleParameter + '\'' +
                ", bodyParameter='" + bodyParameter + '\'' +
                ", dateParameter=" + dateParameter +
                '}';
    }
}
