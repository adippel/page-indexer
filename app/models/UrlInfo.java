package models;

import org.jsoup.nodes.Document;

import java.util.Date;

/**
 * Created by adippel on 21.10.2015.
 */
public class UrlInfo {
    private Document document;
    private Date lastModified;

    public Document getDocument() {
        return document;
    }

    public void setDocument(final Document document) {
        this.document = document;
    }

    public Date getLastModified() {
        return lastModified;
    }

    public void setLastModified(final Date lastModified) {
        this.lastModified = lastModified;
    }

    @Override
    public String toString() {
        return "UrlInfo{" +
                "document=" + document +
                ", lastModified='" + lastModified + '\'' +
                '}';
    }

}
