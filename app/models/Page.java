package models;

import enums.LoadStatus;

import java.net.URL;
import java.util.Set;

/**
 * Created by adippel on 20.10.2015.
 */

public class Page {
    private URL url;
    private LoadStatus status;
    private PageContent content;
    private PageIndexReport indexReport;
    private Set<URL> subUrls;

    public Page() {
    }

    public Page(final URL url, final LoadStatus status) {
        this.url = url;
        this.status = status;
    }

    public URL getUrl() {
        return url;
    }

    public void setUrl(final URL url) {
        this.url = url;
    }

    public LoadStatus getStatus() {
        return status;
    }

    public void setStatus(final LoadStatus status) {
        this.status = status;
    }

    public PageContent getContent() {
        return content;
    }

    public void setContent(final PageContent content) {
        this.content = content;
    }

    public Set<URL> getSubUrls() {
        return subUrls;
    }

    public void setSubUrls(final Set<URL> subUrls) {
        this.subUrls = subUrls;
    }

    public PageIndexReport getIndexReport() {
        return indexReport;
    }

    public void setIndexReport(final PageIndexReport indexReport) {
        this.indexReport = indexReport;
    }

    @Override
    public boolean equals(final Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        final Page page = (Page) o;

        return url.equals(page.url);

    }

    @Override
    public int hashCode() {
        return url.hashCode();
    }

    @Override
    public String toString() {
        return "Page{" +
                "url=" + url +
                ", status=" + status +
                ", content=" + content +
                ", indexReport=" + indexReport +
                ", subUrls=" + subUrls +
                '}';
    }
}
