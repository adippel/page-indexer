package models;

/**
 * Created by adippel on 23.10.2015.
 */
public class Index {
    private String index;

    public Index() {
    }

    public Index(final String index) {
        this.index = index;
    }

    public String getIndex() {
        return index;
    }

    public void setIndex(final String index) {
        this.index = index;
    }

    @Override
    public String toString() {
        return "Index{" +
                "index='" + index + '\'' +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Index index1 = (Index) o;

        return !(index != null ? !index.equals(index1.index) : index1.index != null);

    }

    @Override
    public int hashCode() {
        return index != null ? index.hashCode() : 0;
    }
}
