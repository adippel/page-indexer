package models;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by adippel on 23.10.2015.
 */
public class PageIndexReport {
    private Map<String, Integer> indexMap = new HashMap<>();

    public Map<String, Integer> getIndexMap() {
        return indexMap;
    }

    public void setIndexMap(final Map<String, Integer> indexMap) {
        this.indexMap = indexMap;
    }

    @Override
    public String toString() {
        return "PageIndexReport{" +
                "indexMap=" + indexMap +
                '}';
    }
}
