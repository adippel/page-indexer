package models;

/**
 * Created by adippel on 29.10.2015.
 */

public class ImgResource extends Resource {
    private byte[] content;

    public byte[] getByteContent() {
        return content;
    }

    public void setByteContent(final byte[] content) {
        this.content = content;
    }
}
