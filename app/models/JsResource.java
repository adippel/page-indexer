package models;

/**
 * Created by adippel on 29.10.2015.
 */
public class JsResource extends Resource {
    private String content;

    public String getContent() {
        return content;
    }

    public void setContent(final String content) {
        this.content = content;
    }
}
