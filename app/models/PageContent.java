package models;

import java.util.Date;
import java.util.Set;

/**
 * Created by adippel on 20.10.2015.
 */

public class PageContent {
    private String html;
    private String titleContent;
    private String bodyContent;
    private Set<String> cssList;
    private Set<String> jsList;
    private Set<String> images;
    private Date lastUpdate;

    public String getHtml() {
        return html;
    }

    public void setHtml(final String html) {
        this.html = html;
    }

    public String getTitleContent() {
        return titleContent;
    }

    public void setTitleContent(final String titleContent) {
        this.titleContent = titleContent;
    }

    public String getBodyContent() {
        return bodyContent;
    }

    public void setBodyContent(final String bodyContent) {
        this.bodyContent = bodyContent;
    }

    public Set<String> getCssList() {
        return cssList;
    }

    public void setCssList(Set<String> cssList) {
        this.cssList = cssList;
    }

    public Set<String> getJsList() {
        return jsList;
    }

    public void setJsList(Set<String> jsList) {
        this.jsList = jsList;
    }

    public Set<String> getImages() {
        return images;
    }

    public void setImages(Set<String> images) {
        this.images = images;
    }

    public Date getLastUpdate() {
        return lastUpdate;
    }

    public void setLastUpdate(final Date lastUpdate) {
        this.lastUpdate = lastUpdate;
    }

    @Override
    public String toString() {
        return "PageContent{" +
                "html='" + html + '\'' +
                ", cssList=" + cssList +
                ", jsList=" + jsList +
                ", images=" + images +
                ", lastUpdate=" + lastUpdate +
                '}';
    }
}
