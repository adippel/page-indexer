package utils;

import models.*;
import org.apache.commons.lang3.StringUtils;
import org.jsoup.Connection;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import play.Logger;
import services.DownloadService;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.MalformedURLException;
import java.net.URL;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashSet;
import java.util.Locale;
import java.util.Set;

/**
 * Created by adippel on 20.10.2015.
 */
@Component
public class ContentExtractor {

    private static final SimpleDateFormat LAST_UPDATE_HEADER_FORMAT = new SimpleDateFormat("EEE, dd MMM yyyy HH:mm:ss zzz", Locale.US);

    @Autowired
    DownloadService downloadService;

    public UrlInfo connectToURL(final URL url){
        Logger.trace("Connecting to url " + url);
        try {
            final UrlInfo urlInfo = new UrlInfo();
            final Connection connection = Jsoup.connect(url.toString());
            final Document page = connection.get();
            final String lastModifiedStr = connection.response().header("Last-Modified");
            Logger.debug("Last modified string: " + lastModifiedStr);
            if(StringUtils.isNotEmpty(lastModifiedStr)){
                final Date lastModifiedDate = parseDate(lastModifiedStr);
                urlInfo.setLastModified(lastModifiedDate);
            }
            urlInfo.setDocument(page);
            return urlInfo;
        } catch (final IOException e) {
            Logger.warn("Coudn't get content from url: " + url);
            return null;
        }
    }

    public PageContent extractContent(final UrlInfo urlInfo) {
        Logger.debug("Extracting content");
        final PageContent pageContent = new PageContent();
        final Document document = urlInfo.getDocument();
        Logger.debug("Extracting html");
        final String html = extractHtml(document);
        pageContent.setHtml(html);
        Logger.debug("Extracting title");
        final String titleContent = extractTitleContent(document);
        pageContent.setTitleContent(titleContent);
        Logger.debug("Extracting body");
        final String bodyContent = extractBodyContent(document);
        pageContent.setBodyContent(bodyContent);
        Logger.debug("Extracting last update");
        final Date lastUpdate =urlInfo.getLastModified();
        pageContent.setLastUpdate(lastUpdate);
        Logger.debug("Extracting images");
        final Set<String> images = extractImgSet(document);
        pageContent.setImages(images);
        Logger.debug("Extracting scripts");
        final Set<String> scripts = extractJsSet(document);
        pageContent.setJsList(scripts);
        Logger.debug("Extracting css");
        final Set<String> cssSet = extractCssSet(document);
        pageContent.setCssList(cssSet);
        return pageContent;
    }

    private String extractHtml(final Document document){
        return document.html();
    }

    private Set<String> extractJsSet(final Document document ) {
        final Elements srcElements = document.select("script[src]");
        final Set<String> scriptSources = new HashSet<>();
        for(final Element srcElement : srcElements){
            final String scriptSource = srcElement.attr("abs:src");
            if(StringUtils.isNotEmpty(scriptSource)) {
                scriptSources.add(scriptSource);
            }
        }

        final Set<String> downloadedScripts = new HashSet<>();
        for(final String scriptSource : scriptSources){
            Logger.debug("Script sources: " + scriptSource);
            final JsResource jsResource;
            try {
                jsResource = downloadService.downloadScript(scriptSource);
                if(jsResource!=null){
                    downloadedScripts.add(scriptSource);
                }
            } catch (IOException e) {
               Logger.warn(e.getMessage());
            }

        }
        return downloadedScripts;
    }

    private Set<String> extractCssSet(final Document document){
        final Elements cssElements = document.select("link[rel=stylesheet]");
        final Set<String> cssSources = new HashSet<>();
        for(final Element cssElement : cssElements){
            final String cssSource = cssElement.attr("abs:href");
            if(StringUtils.isNotEmpty(cssSource)){
                cssSources.add(cssSource);
            }
        }

        final Set<String> downloadedCssSet = new HashSet<>();
        for(final String cssSource : cssSources){
            Logger.debug("Css source "+cssSource);
            final CssResource cssResource;
            try {
                cssResource = downloadService.downloadCss(cssSource);
                if(cssResource!=null){
                    downloadedCssSet.add(cssSource);
                }
            } catch (IOException e) {
                Logger.warn(e.getMessage());
            }

        }
        return downloadedCssSet;
    }

    private Set<String> extractImgSet(final Document document) {
        final Elements imgElements = document.select("[src]");
        final Set<String> imageSources = new HashSet<>();
        for(final Element imgElement : imgElements){
            if(imgElement.tagName().equals("img")) {
                final String imgSrc = imgElement.attr("abs:src");
                if(isImage(imgSrc)){
                    imageSources.add(imgSrc);
                }
            }
        }
        final Set<String> images = new HashSet<>();
        for(final String imageSource : imageSources) {
            final ImgResource imgResource;
            try {
                imgResource = downloadService.downloadImage(imageSource);
                if(imgResource!=null){
                    images.add(imageSource);
                }
            } catch (IOException e) {
                Logger.warn(e.getMessage());
            }

        }
        return images;
    }

    private String extractTitleContent(final Document document){
        return document.title();
    }

    private String extractBodyContent(final Document document){
        return document.body().text();
    }

    public Set<URL> extractDomainSubUrls(final Document document, final URL sourceUrl){
        Logger.trace("Extracting suburls from: "+sourceUrl);
        final Elements subUrlElements = document.select("a[href]");
        final String domain = sourceUrl.getHost();
        final Set<URL> subUrls = new HashSet<>();
        for(final Element urlElement: subUrlElements){
            final String subUrlStr = urlElement.attr("abs:href");
            if(StringUtils.isNotEmpty(subUrlStr)) {
                try {
                    final URL subUrl = new URL(subUrlStr);
                    final String subUrlDomain = subUrl.getHost();
                    if (subUrlDomain.contains(domain)) {
                        subUrls.add(subUrl);
                    }
                } catch (final MalformedURLException e) {
                    Logger.warn("Coudn't get subURL content from url: " + sourceUrl + " by subURL: " + subUrlStr);
                }
            }
        }
        return subUrls;
    }

    private boolean isImage(final String imgSrc){
       return(imgSrc != null &&
                (imgSrc.endsWith(".jpg")
                || (imgSrc.endsWith(".png"))
                || (imgSrc.endsWith(".jpeg"))
                || (imgSrc.endsWith(".bmp"))
                || (imgSrc.endsWith(".ico"))));
    }

    private String downloadScript(final String source){
        try {
            final URL sourceUrl = new URL(source);
            final BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(sourceUrl.openStream()));
            final StringBuilder scriptStringBuilder = new StringBuilder();
            String scriptString;
            while((scriptString = bufferedReader.readLine())!=null){
                scriptStringBuilder.append(scriptString);
            }
            return scriptStringBuilder.toString();

        } catch (final IOException ex) {
            Logger.warn("Couldn't connect to script source: " + source);
            return null;
        }
    }

    private Date parseDate(final String dateStr){
        try {
            final Date date = LAST_UPDATE_HEADER_FORMAT.parse(dateStr);
            return date;
        } catch (final ParseException e) {
            Logger.warn(e.toString());
            return null;
        }
    }


}
