package services;

import java.io.IOException;

/**
 * Created by adippel on 26.10.2015.
 */
public interface PageIndexingService {

    void beginIndexing() throws IOException;
}
