package services;

import javassist.NotFoundException;
import models.Index;

import java.io.IOException;
import java.util.Set;

/**
 * Created by adippel on 23.10.2015.
 */
public interface IndexService {

    void addIndexes(Set<Index> indexes) throws IOException;
    void deleteIndex(Index index) throws NotFoundException, IOException;
    Set<Index> findAll() throws IOException;

    void clear() throws IOException;
}
