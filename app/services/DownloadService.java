package services;

import models.CssResource;
import models.ImgResource;
import models.JsResource;

import java.io.IOException;

/**
 * Created by adippel on 29.10.2015.
 */
public interface DownloadService {
    ImgResource downloadImage(String imgSource) throws IOException;
    JsResource downloadScript(String scriptSource) throws IOException;
    CssResource downloadCss(String cssSource) throws IOException;

}
