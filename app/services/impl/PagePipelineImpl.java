package services.impl;

import daos.IndexDao;
import daos.PageDao;
import enums.LoadStatus;
import javassist.NotFoundException;
import models.*;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;
import play.Logger;
import services.PagePipeline;
import utils.ContentExtractor;

import java.io.IOException;
import java.net.URL;
import java.util.HashMap;
import java.util.Set;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.locks.ReentrantLock;

/**
 * Created by adippel on 23.10.2015.
 */
@Component("pagePipeline")
public class PagePipelineImpl implements PagePipeline {

    private static final ReentrantLock urlAddQueueLock = new ReentrantLock();
    private static BlockingQueue<IteratedUrl> urlAddQueue = new LinkedBlockingQueue<>();
    private static BlockingQueue<URL> urlIndexingQueue = new LinkedBlockingQueue<>();
    private ExecutorService loadPageExecutorService;
    private ExecutorService urlAddQueueListener = Executors.newSingleThreadExecutor();
    private ExecutorService indexPageExecutorService;
    private ExecutorService urlIndexQueueListener = Executors.newSingleThreadExecutor();


    @Autowired
    @Qualifier("primaryPageDao")
    private PageDao pageDao;

    @Autowired
    @Qualifier("primaryIndexDao")
    private IndexDao indexDao;

    @Autowired
    private ContentExtractor contentExtractor;

    @Autowired
    public PagePipelineImpl(@Value("${loadExecutor.ThreadPool}") final int loadExecutorThreadPool, @Value("${indexingExecutor.ThreadPool}") final int indexPageExecutorThreadPool) {
        this.loadPageExecutorService = Executors.newFixedThreadPool(loadExecutorThreadPool);
        this.indexPageExecutorService = Executors.newFixedThreadPool(indexPageExecutorThreadPool);
    }

    @Override
    public void addToAddingQueue(final IteratedUrl iteratedUrl){
        urlAddQueue.add(iteratedUrl);
    }

    @Override
    public void addToIndexingQueue(URL url) {
        urlIndexingQueue.add(url);
    }

    @Override
    public void initListeners(){
        urlAddQueueListener.submit(() -> {
            while(true){
                try {
                    final IteratedUrl iteratedUrl = urlAddQueue.take();
                    lockAddQueue();
                    loadPageExecutorService.submit(() -> {
                                try {
                                    loadPage(iteratedUrl.getUrl());
                                } catch (NotFoundException | IOException e) {
                                    e.printStackTrace();
                                }

                            }
                    );

                } catch (final InterruptedException e) {
                    Logger.warn("Interrupted exception while listening to queue " + e.getLocalizedMessage());
                } finally {
                    unlockAddQueue();
                }
            }
        });

        urlIndexQueueListener.submit(() -> {
           while(true){
               try {
                   final URL indexingUrl = urlIndexingQueue.take();
                   loadPageExecutorService.submit(()->{
                               try {
                                   indexPage(indexingUrl);
                               } catch (NotFoundException e) {
                                   Logger.warn(e.getMessage());
                               } catch (IOException e) {
                                   Logger.warn(e.getMessage());
                               }
                           }
                   );
               } catch (final InterruptedException e) {
                   Logger.warn("Interrupted exception while listening to queue " + e.getLocalizedMessage());
               }
           }
        });
    }

    @Override
    public void lockAddQueue(){
        urlAddQueueLock.lock();
    }

    @Override
    public void unlockAddQueue(){
        urlAddQueueLock.unlock();
    }

    @Override
    public void shutDownListeners(){
        urlAddQueueListener.shutdownNow();
        urlIndexQueueListener.shutdownNow();
        loadPageExecutorService.shutdownNow();
        indexPageExecutorService.shutdownNow();
    }

    @Override
    public int getAddingQueueSize() {
        return urlAddQueue.size();
    }

    @Override
    public int getIndexingQueueSize() {
        return urlIndexingQueue.size();
    }

    @Override
    public void removeFromAddQueue(Set<URL> urls){
        for(URL url : urls){
            urlAddQueue.remove(new IteratedUrl(url, 0));
        }
    }

    private void loadPage(final URL sourceUrl) throws NotFoundException, IOException {
        Logger.info("Loading page" + sourceUrl);
        final UrlInfo urlInfo = contentExtractor.connectToURL(sourceUrl);
        if(urlInfo == null){
            pageDao.findPageAndRemove(sourceUrl);
            return;
        }

        Page page = new Page();
        page.setUrl(sourceUrl);

        final Set<URL> subUlrs = contentExtractor.extractDomainSubUrls(urlInfo.getDocument(), sourceUrl);
        page.setSubUrls(subUlrs);

        final PageContent pageContent;
        Logger.info("Extracting content " + sourceUrl);
        pageContent = contentExtractor.extractContent(urlInfo);
        page.setContent(pageContent);
        page.setStatus(LoadStatus.READY);

        Logger.info("Updating full content " + sourceUrl);
        pageDao.updateFullContent(page);
        addToIndexingQueue(page.getUrl());

    }

    private void indexPage(final URL url) throws NotFoundException, IOException {
        Logger.debug("Indexing page with url "+ url);
        Set<Index> indexes = indexDao.findAll();
        Logger.debug("Got indexes "+indexes.size());
        if(indexes.isEmpty()){
            Logger.debug("No indexes in db");
            return;
        }

        pageDao.updateStatus(url, LoadStatus.INDEXING);
        Page indexingPage = pageDao.getPageContentAndSubUrls(url);
        Logger.debug("Got indexing page "+ url);
        Logger.debug("Got indexes "+ indexes);
        final String titleContent = indexingPage.getContent().getTitleContent();
        final String bodyContent = indexingPage.getContent().getBodyContent();

        final PageIndexReport pageIndexReport = new PageIndexReport();
        final HashMap<String, Integer> indexesMap = new HashMap<>();

        for (final Index index : indexes) {
            final int indexCount = StringUtils.countMatches((titleContent + bodyContent).toLowerCase(), index.getIndex().toLowerCase());
            if (indexCount > 0) {
                indexesMap.put(index.getIndex(), indexCount);
            }
        }

        pageIndexReport.setIndexMap(indexesMap);
        Logger.trace("Index Map: " + indexesMap.toString());
        final Page indexedPage = new Page(indexingPage.getUrl(), LoadStatus.READY);
        indexedPage.setIndexReport(pageIndexReport);
        pageDao.updateIndexReport(indexedPage);
    }







}
