package services.impl;

import daos.CssDao;
import daos.ImgDao;
import daos.JsDao;
import models.CssResource;
import models.ImgResource;
import models.JsResource;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;
import play.Logger;
import services.DownloadService;

import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.io.BufferedReader;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.URL;

/**
 * Created by adippel on 29.10.2015.
 */
@Service
public class DownloadServiceImpl implements DownloadService {

    @Autowired
    @Qualifier("primaryImgDao")
    ImgDao imgDao;

    @Autowired
    @Qualifier("primaryJsDao")
    JsDao jsDao;

    @Autowired
    @Qualifier("primaryCssDao")
    CssDao cssDao;

    @Override
    public ImgResource downloadImage(final String imgSource) throws IOException {
        Logger.debug("got image source " + imgSource);
        ImgResource imgResource = imgDao.find(imgSource);
        Logger.debug("image found");
        if(imgResource == null){
            imgResource = new ImgResource();

            try(ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream()){
                final URL imageUrl = new URL(imgSource);
                Logger.debug("downloading " + imgSource);
                final BufferedImage image = ImageIO.read(imageUrl);
                String format = imgSource.substring(imgSource.lastIndexOf(".")+1);

                ImageIO.write(image, format, byteArrayOutputStream);
                byteArrayOutputStream.flush();
                final byte[] imageAsRawBytes = byteArrayOutputStream.toByteArray();
                byteArrayOutputStream.close();
                imgResource.setByteContent(imageAsRawBytes);
                imgResource.setSource(imgSource);
                Logger.debug("saving img source" + imgSource);
                imgDao.save(imgResource);
            } catch (final IOException ex) {
                Logger.warn("Couldn't download image from " + imgSource + "while downloading");
                throw ex;
            }
        }
        return imgResource;
    }

    @Override
    public JsResource downloadScript(final String scriptSource) throws IOException {
        JsResource jsResource = jsDao.find(scriptSource);
        if(jsResource == null){
            jsResource = new JsResource();
            final URL sourceUrl = new URL(scriptSource);
            try(BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(sourceUrl.openStream()))) {
                final StringBuilder scriptStringBuilder = new StringBuilder();
                String scriptString;
                while((scriptString = bufferedReader.readLine())!=null){
                    scriptStringBuilder.append(scriptString);
                }
                jsResource.setContent(scriptStringBuilder.toString());
                jsResource.setSource(scriptSource);
                jsDao.save(jsResource);
            } catch (final IOException ex) {
                Logger.warn("Couldn't connect to script source: " + scriptSource);
                throw ex;
            }
        }
        return jsResource;
    }

    @Override
    public CssResource downloadCss(final String cssSource) throws IOException {
        CssResource cssResource = cssDao.find(cssSource);
        if(cssResource==null){

                cssResource = new CssResource();
                final URL sourceUrl = new URL(cssSource);
            try(BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(sourceUrl.openStream()))) {
                final StringBuilder cssStringBuilder = new StringBuilder();
                String scriptString;
                while((scriptString = bufferedReader.readLine())!=null){
                    cssStringBuilder.append(scriptString);
                }
                cssResource.setContent(cssStringBuilder.toString());
                cssResource.setSource(cssSource);
                cssDao.save(cssResource);
            } catch (final IOException ex) {
                Logger.warn("Couldn't connect to css source: " + cssSource);
                throw ex;
            }
        }
        return cssResource;
    }
}
