package services.impl;

import daos.CssDao;
import daos.ImgDao;
import daos.JsDao;
import daos.PageDao;
import models.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import play.Logger;
import services.ExportService;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.net.URL;
import java.nio.charset.StandardCharsets;
import java.util.Set;
import java.util.zip.ZipEntry;
import java.util.zip.ZipOutputStream;

/**
 * Created by adippel on 27.10.2015.
 */

@Service
public class ExportServiceImpl implements ExportService {
    @Autowired
    @Qualifier("primaryImgDao")
    ImgDao imgDao;
    @Autowired
    @Qualifier("primaryJsDao")
    JsDao jsDao;
    @Autowired
    @Qualifier("primaryCssDao")
    CssDao cssDao;
    @Value("${archive.directory}")
    private String archiveDirectory;
    @Autowired
    @Qualifier("primaryPageDao")
    private PageDao pageDao;

    @Override
    public void addToArchive(final URL url) throws IOException{
        Logger.info("Adding url " + url + "and its subpages content to zip archive");
        final Page page = pageDao.getPageContentAndSubUrls(url);
        if(page ==null){
            return;
        }
        final PageContent pageContent = page.getContent();
        if(pageContent==null){
            return;
        }
        final File f = new File(archiveDirectory + prepareUrl(page.getUrl().toString()) + ".zip");
        final FileOutputStream fileOutputStream= new FileOutputStream(f);

        try(final ZipOutputStream zipOutputStream = new ZipOutputStream(fileOutputStream)) {

            addStringToArchive(zipOutputStream, prepareUrl(url.toString()) + ".html", pageContent.getHtml());

            final Set<String> imageSources = pageContent.getImages();
            addImagesToArchive(zipOutputStream, imageSources, "/images/");

            final Set<String> scriptSources = pageContent.getJsList();
            addScriptsToArchive(zipOutputStream, scriptSources, "/scripts/");

            final Set<String> cssSources = pageContent.getCssList();
            addCssSetToArchive(zipOutputStream, cssSources, "/css/");

            final Set<URL> subUrls = page.getSubUrls();
            for (final URL subUrl : subUrls) {
                try {
                    addSubUrlToArchive(zipOutputStream, subUrl);
                } catch (IOException e) {
                    Logger.warn("Exception while exporting suburl: " + e.getMessage());
                }
            }
        }catch (IOException e){
            Logger.warn("Exception while exporting url " + url);
            throw e;
        }

    }


    private void addSubUrlToArchive(final ZipOutputStream zipOutputStream, final URL subUrl) throws IOException{
        Logger.debug("Adding subUrl " + subUrl + " content to archive");
        final Page subPage = pageDao.getPageContentAndSubUrls(subUrl);
        if(subPage==null){
            return;
        }
        final PageContent subPageContent = subPage.getContent();
        if(subPageContent!=null) {
            final String path = "subpages/" + prepareUrl(subUrl.toString()) + "/";

            addStringToArchive(zipOutputStream, path + prepareUrl(subUrl.toString()) + ".html", subPageContent.getHtml());

            final Set<String> subPageImageSources = subPageContent.getImages();
            addImagesToArchive(zipOutputStream, subPageImageSources, path + "images/");

            final Set<String> subPageScriptSources = subPageContent.getJsList();
            addScriptsToArchive(zipOutputStream, subPageScriptSources, path + "scripts/");

            final Set<String> subPageCssSources = subPageContent.getCssList();
            addCssSetToArchive(zipOutputStream, subPageCssSources, path + "css/");
        }
    }

    private void addStringToArchive(final ZipOutputStream zipOutputStream, final String path, final String text) throws IOException {
        final ZipEntry zipEntry = new ZipEntry(path);
        zipOutputStream.putNextEntry(zipEntry);
        zipOutputStream.write(text.getBytes(StandardCharsets.UTF_8));
        zipOutputStream.closeEntry();
    }

    private void addScriptsToArchive(final ZipOutputStream zipOutputStream, final Set<String> scriptSources, final String path) throws IOException {
        for(final String scriptSource : scriptSources){
            final JsResource jsResource = jsDao.find(scriptSource);
            if(jsResource !=null) {
                final String scriptName = scriptSource.substring(scriptSource.lastIndexOf("/") + 1, scriptSource.length());
                addStringToArchive(zipOutputStream, path + scriptName, jsResource.getContent());
            }

        }
    }

    private void addCssSetToArchive(final ZipOutputStream zipOutputStream, final Set<String> cssSources, final String path) throws IOException {
        for(final String cssSource : cssSources){
            final CssResource cssResource = cssDao.find(cssSource);
            if(cssResource != null) {
                final String cssName = cssSource.substring(cssSource.lastIndexOf("/") + 1, cssSource.length());
                addStringToArchive(zipOutputStream, path + cssName, cssResource.getContent());
            }
        }
    }

    private void addImagesToArchive(final ZipOutputStream zipOutputStream, final Set<String> imgSources, final String path) throws IOException {
        if(imgSources.isEmpty()){
            return;
        }
        for (final String imgSource : imgSources) {
            Logger.debug("imgsource : " + imgSource);
            final ImgResource imgResource = imgDao.find(imgSource);
            if(imgResource != null) {
                final String imgName = imgSource.substring(imgSource.lastIndexOf("/") + 1, imgSource.length());
                addImageToArchive(zipOutputStream, path + imgName, imgResource.getByteContent());
            }
        }
    }

    private void addImageToArchive(final ZipOutputStream zipOutputStream, final String path, final byte[] image) throws IOException {
        final ZipEntry zipEntry = new ZipEntry(path);
        try {
            zipOutputStream.putNextEntry(zipEntry);
            zipOutputStream.write(image);
            zipOutputStream.closeEntry();
        } catch (final IOException e) {
            Logger.warn("Couldn't save image to zip archive " + path);
            throw e;
        }
    }

    private String prepareUrl(final String url){
        return url.replace(".", "").replace("http://", "").replace("https://", "").replace("/","_");
    }
}
