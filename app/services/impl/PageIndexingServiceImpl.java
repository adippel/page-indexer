package services.impl;

import daos.IndexDao;
import daos.PageDao;
import enums.LoadStatus;
import models.Index;
import models.Page;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import play.Logger;
import services.PageIndexingService;
import services.PagePipeline;

import java.io.IOException;
import java.util.List;
import java.util.Set;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

/**
 * Created by adippel on 26.10.2015.
 */
@Service
public class PageIndexingServiceImpl implements PageIndexingService{
    private static Set<Index> indexes;

    @Autowired
    @Qualifier("primaryIndexDao")
    private IndexDao indexDao;

    @Autowired
    @Qualifier("primaryPageDao")
    private PageDao pageDao;

    @Autowired
    private PagePipeline pagePipeline;

    @Value("${loadpages.limit}")
    private int limit;

    private ExecutorService indexingExecutorService;

    @Autowired
    public PageIndexingServiceImpl(@Value("${indexingExecutor.ThreadPool}") final int indexingExecutorThreadPool) {
        this.indexingExecutorService = Executors.newFixedThreadPool(indexingExecutorThreadPool);
    }

    @Override
    public void beginIndexing() throws IOException {
        Logger.info("Beginning indexing");
        indexes = indexDao.findAll();

        final long readyPageCount = pageDao.getPageCountByStatus(LoadStatus.READY);

        final int iterationsCount =(int) Math.ceil(((double) readyPageCount)/limit);

        for(int currentIteration = 0; currentIteration < iterationsCount; currentIteration++){
            Logger.debug("Iteration count "+ currentIteration);
            final List<Page> pages = pageDao.getPartialPagesWithTextContentByStatus(LoadStatus.READY, currentIteration*limit, limit);
            for(final Page page : pages){
                Logger.debug("Page " + page.getUrl());
                pagePipeline.addToIndexingQueue(page.getUrl());
            }
        }
    }

}
