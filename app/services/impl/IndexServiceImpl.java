package services.impl;

import daos.IndexDao;
import models.Index;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;
import services.IndexService;

import java.io.IOException;
import java.util.Set;

/**
 * Created by adippel on 23.10.2015.
 */
@Service
public class IndexServiceImpl implements IndexService {

    @Autowired
    @Qualifier("primaryIndexDao")
    IndexDao indexDao;

    @Override
    public void addIndexes(final Set<Index> indexes) throws IOException {
        for(final Index index : indexes){
            indexDao.save(index);
        }
    }

    @Override
    public void deleteIndex(final Index index) throws IOException {
        indexDao.delete(index);
    }

    @Override
    public Set<Index> findAll() throws IOException {
        return indexDao.findAll();
    }

    @Override
    public void clear() throws IOException {
        indexDao.clear();
    }
}
