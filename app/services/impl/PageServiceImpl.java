package services.impl;

import daos.CssDao;
import daos.ImgDao;
import daos.JsDao;
import daos.PageDao;
import enums.LoadStatus;
import javassist.NotFoundException;
import models.*;
import models.exceptions.AlreadyExistsException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import play.Logger;
import services.PageService;
import utils.ContentExtractor;

import java.io.IOException;
import java.net.URL;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

/**
 * Created by adippel on 20.10.2015.
 */

@Service
public class PageServiceImpl implements PageService {

    @Autowired
    ContentExtractor contentExtractor;
    @Autowired
    @Qualifier("primaryImgDao")
    ImgDao imgDao;
    @Autowired
    @Qualifier("primaryJsDao")
    JsDao jsDao;
    @Autowired
    @Qualifier("primaryCssDao")
    CssDao cssDao;
    @Autowired
    PagePipelineImpl pagePipeline;
    @Autowired
    @Qualifier("primaryPageDao")
    private PageDao pageDao;

    @Value("${search.depth}")
    private int searchDepth;


    @Override
    public void addPage(final URL url) throws AlreadyExistsException, IOException {
        Logger.trace("Adding page " + url + " to queue");

        final Page page = new Page(url, LoadStatus.DOWNLOADING);
        Logger.debug("Inserting page " + page + " in db");
        pageDao.insert(page);

        final UrlInfo urlInfo = contentExtractor.connectToURL(url);
        if(urlInfo == null){
            Logger.warn("Loading url "+ url + " failed");
            return;
        }
        final Set<URL> subUrls = contentExtractor.extractDomainSubUrls(urlInfo.getDocument(), url);
        final IteratedUrl iteratedUrl = new IteratedUrl(url, 0);
        pagePipeline.addToAddingQueue(iteratedUrl);
        final int currentIteration = iteratedUrl.getIterationRound();
        Logger.debug("Url " + url + " added to queue");

        for(final URL subUrl : subUrls){

            Page subpage = new Page(subUrl, LoadStatus.DOWNLOADING);
            try {

                pageDao.insert(subpage);

                final IteratedUrl iteratedSubUrl = new IteratedUrl(subUrl, currentIteration + 1);
                pagePipeline.addToAddingQueue(iteratedSubUrl);
                Logger.debug("SubUrl " + subUrl + " added to queue");
            }catch (AlreadyExistsException e){
                Logger.warn("Subpage already exists: "+ e.getMessage());
            } catch (IOException e) {
                Logger.warn(e.getMessage());
            }


        }
    }

    @Override
    public LoadStatus getStatus(final URL url) throws NotFoundException, IOException {
        Logger.trace("Getting url status for url " + url);
        final LoadStatus loadStatus = pageDao.getStatus(url);
        Logger.debug("Url " + url + "status is " + loadStatus);
        return loadStatus;
    }

    @Override
    public void deletePage(final URL url) throws NotFoundException, IOException {
        final Set<String> imagesToDelete = new HashSet<>();
        final Set<String> scriptsToDelete = new HashSet<>();
        final Set<String> cssSetToDelete = new HashSet<>();

        final LoadStatus loadStatus  = pageDao.getStatus(url);
        if(loadStatus == LoadStatus.DOWNLOADING){
            throw new NotFoundException("Page is still downloading");
        }
        Page page = pageDao.findPageAndRemove(url);

        try{
            pagePipeline.lockAddQueue();
            pagePipeline.removeFromAddQueue(page.getSubUrls());
        }finally {
            pagePipeline.unlockAddQueue();
        }

        final PageContent pageContent = page.getContent();
        if(pageContent == null){
            throw new NotFoundException("Page doesn't have content");
        }
        if(!pageContent.getImages().isEmpty()) {
            imagesToDelete.addAll(pageContent.getImages());
        }
        if(!pageContent.getJsList().isEmpty()) {
            scriptsToDelete.addAll(pageContent.getJsList());
        }
        if(!pageContent.getCssList().isEmpty()) {
            cssSetToDelete.addAll(pageContent.getCssList());
        }
        for(final URL subUrl : page.getSubUrls()){
            try {
                final Page subPage = pageDao.findPageAndRemove(subUrl);
                if(subPage != null) {
                    final PageContent subPageContent = subPage.getContent();
                    if (subPageContent != null) {
                        if (subPageContent.getImages() != null) {
                            imagesToDelete.addAll(subPageContent.getImages());
                        }
                        if (subPageContent.getJsList() != null) {
                            scriptsToDelete.addAll(subPageContent.getJsList());
                        }
                        if (subPageContent.getCssList() != null) {
                            cssSetToDelete.addAll(subPageContent.getCssList());
                        }
                    }
                }
            }catch (IOException e){
                Logger.warn(e.getMessage());
            }
        }

        Logger.info("images size : "+ imagesToDelete.size());
        Logger.info("css size : "+ cssSetToDelete.size());
        Logger.info("js size : "+ scriptsToDelete.size());

        for(final String imageToDelete : imagesToDelete){
            Logger.info("Image to delete: " + imageToDelete);
            try {
                imgDao.delete(imageToDelete);
            } catch (IOException e) {
                Logger.warn(e.getMessage());
            }
        }
        for(final String scriptToDelete : scriptsToDelete){
            try {
                jsDao.delete(scriptToDelete);
            } catch (IOException e) {
                Logger.warn(e.getMessage());
            }
        }
        for(final String cssToDelete : cssSetToDelete){
            try {
                cssDao.delete(cssToDelete);
            } catch (IOException e) {
                Logger.warn(e.getMessage());
            }
        }
    }

    @Override
    public PageIndexReport getPageIndexReport(final URL url) throws NotFoundException, IOException {
        final PageIndexReport pageIndexReport = pageDao.getIndexReport(url);
        return pageIndexReport;
    }

    @Override
    public List<Page> getFilteredPages(final FilterParameter filterParameter) throws IOException {
        return pageDao.getFilteredPageList(filterParameter);
    }

    @Override
    public void clearAllData() throws IOException {
        pageDao.clear();
        imgDao.clear();
        cssDao.clear();
        jsDao.clear();
    }


}
