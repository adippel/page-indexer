package services;

import enums.LoadStatus;
import javassist.NotFoundException;
import models.FilterParameter;
import models.Page;
import models.PageIndexReport;
import models.exceptions.AlreadyExistsException;

import java.io.IOException;
import java.net.URL;
import java.util.List;

/**
 * Created by adippel on 20.10.2015.
 */

public interface PageService {

    void addPage(URL url) throws AlreadyExistsException, IOException;
    LoadStatus getStatus(URL url) throws NotFoundException, IOException;
    void deletePage(URL url) throws NotFoundException, IOException;
    PageIndexReport getPageIndexReport(URL url) throws NotFoundException, IOException;
    List<Page> getFilteredPages(FilterParameter filterParameter) throws IOException;

    //for test purpose
    void clearAllData() throws IOException;

}
