package services;

import models.IteratedUrl;

import java.net.URL;
import java.util.Set;

/**
 * Created by adippel on 23.10.2015.
 */
public interface PagePipeline {

    void addToAddingQueue(IteratedUrl iteratedUrl);

    void lockAddQueue();
    void unlockAddQueue();
    void removeFromAddQueue(Set<URL> urls);
    void addToIndexingQueue(URL url);
    void initListeners();
    void shutDownListeners();

    int getAddingQueueSize();
    int getIndexingQueueSize();
}
