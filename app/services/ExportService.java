package services;

import javassist.NotFoundException;

import java.io.IOException;
import java.net.URL;

/**
 * Created by adippel on 27.10.2015.
 */
public interface ExportService {

    void addToArchive(URL url) throws IOException, NotFoundException;
}
