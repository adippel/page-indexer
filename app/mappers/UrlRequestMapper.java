package mappers;

import com.fasterxml.jackson.databind.JsonNode;
import models.requestmodels.UrlRequest;
import org.springframework.stereotype.Component;

import java.net.MalformedURLException;
import java.net.URL;
import java.util.zip.DataFormatException;

/**
 * Created by adippel on 22.10.2015.
 */
@Component
public class UrlRequestMapper {

    public UrlRequest deserialize(final JsonNode jsonNode) throws DataFormatException {
        final UrlRequest urlRequest = new UrlRequest();
        final String urlStr = jsonNode.findPath("url").asText();
        if(urlStr.isEmpty()) {
            throw new DataFormatException("Missing parameter [url]");
        }
        URL url = null;
        try {
            url = new URL(urlStr);
        } catch (final MalformedURLException e) {
            throw new DataFormatException("Malformed url "+ urlStr);
        }
        urlRequest.setUrl(url);
        return urlRequest;
    }

}
