package mappers;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ArrayNode;
import com.fasterxml.jackson.databind.node.ObjectNode;
import models.Page;
import org.springframework.stereotype.Component;
import play.libs.Json;

import java.util.List;

/**
 * Created by adippel on 27.10.2015.
 */
@Component
public class PagesResponseMapper {
    public ObjectNode serialize(final List<Page> pages){
        final ObjectNode result = Json.newObject();
        final ObjectMapper mapper = new ObjectMapper();
        final ArrayNode array = mapper.createArrayNode();

        for(final Page page : pages){
            final ObjectNode pageNode = Json.newObject();
            pageNode.put("url", page.getUrl().toString());
            pageNode.put("status", page.getStatus().toString());
            array.add(pageNode);
        }
        result.put("pages", array);
        return result;
    }
}
