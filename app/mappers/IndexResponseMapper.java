package mappers;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ArrayNode;
import com.fasterxml.jackson.databind.node.ObjectNode;
import models.Index;
import org.springframework.stereotype.Component;
import play.libs.Json;

import java.util.Set;

/**
 * Created by adippel on 26.10.2015.
 */
@Component
public class IndexResponseMapper {
    public ObjectNode serialize(final Set<Index> indexes){
        final ObjectNode result = Json.newObject();
        final ObjectMapper mapper = new ObjectMapper();
        final ArrayNode array = mapper.createArrayNode();

        for(final Index index : indexes){
            array.add(index.getIndex());
        }
        result.put("indexes", array);
        return result;
    }
}
