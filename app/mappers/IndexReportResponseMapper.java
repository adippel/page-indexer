package mappers;

import com.fasterxml.jackson.databind.node.ObjectNode;
import models.PageIndexReport;
import org.springframework.stereotype.Component;
import play.libs.Json;

import java.util.Map;
import java.util.Set;

/**
 * Created by adippel on 26.10.2015.
 */
@Component
public class IndexReportResponseMapper {

    public ObjectNode serialize(final PageIndexReport pageIndexReport){
        final ObjectNode result = Json.newObject();
        final Map<String, Integer> indexMap = pageIndexReport.getIndexMap();
        final ObjectNode jsonNode = Json.newObject();

        final Set<String> keys = indexMap.keySet();
        for (final String key : keys) {
            jsonNode.put(key, indexMap.get(key));
        }
        result.put("indexReport", jsonNode);
        return result;
    }
}
