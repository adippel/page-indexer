package mappers;

import com.fasterxml.jackson.databind.node.ObjectNode;
import enums.LoadStatus;
import org.springframework.stereotype.Component;
import play.libs.Json;

/**
 * Created by adippel on 22.10.2015.
 */
@Component
public class StatusResponseMapper {

    public ObjectNode serialize(LoadStatus loadStatus){
        final ObjectNode result = Json.newObject();
        result.put("status", loadStatus.toString());
        return result;
    }
}
