package mappers;

import com.fasterxml.jackson.databind.JsonNode;
import models.Index;
import models.requestmodels.UrlRequest;
import org.springframework.stereotype.Component;

import java.util.HashSet;
import java.util.Set;
import java.util.zip.DataFormatException;

/**
 * Created by adippel on 23.10.2015.
 */
@Component
public class IndexRequestMapper {

    public Set<Index> deserializeArray(final JsonNode jsonNode) throws DataFormatException {
        final UrlRequest urlRequest = new UrlRequest();
        final Set<Index> indexes = new HashSet<>();
        final JsonNode indexesNode = jsonNode.get("indexes");
        if(indexesNode == null){
            throw new DataFormatException("Missing parameter [indexes]");
        }
        if(indexesNode.isArray()) {
            for (final JsonNode indexNode : indexesNode) {
                final Index index = new Index(indexNode.asText());
                indexes.add(index);
            }
        }else{
            throw new DataFormatException("Indexes should be array");
        }
        return indexes;
    }

    public Index deserialize(final JsonNode jsonNode) throws DataFormatException{
        final Index index = new Index();
        final String indexStr = jsonNode.findPath("index").asText();
        if(indexStr.isEmpty()) {
            throw new DataFormatException("Missing parameter [index]");
        }
        index.setIndex(indexStr);
        return index;
    }
}
