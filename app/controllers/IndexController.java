package controllers;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.node.ObjectNode;
import javassist.NotFoundException;
import mappers.IndexRequestMapper;
import mappers.IndexResponseMapper;
import models.Index;
import org.springframework.beans.factory.annotation.Autowired;
import play.mvc.Result;
import services.IndexService;
import services.PageIndexingService;

import java.io.IOException;
import java.util.Set;
import java.util.zip.DataFormatException;

import static play.mvc.Controller.request;
import static play.mvc.Results.badRequest;
import static play.mvc.Results.ok;

/**
 * Created by adippel on 23.10.2015.
 */

@org.springframework.stereotype.Controller
public class IndexController {
    @Autowired
    IndexService indexService;
    @Autowired
    IndexRequestMapper indexRequestMapper;
    @Autowired
    IndexResponseMapper indexResponseMapper;

    @Autowired
    PageIndexingService pageIndexingService;

    public Result addIndexes(){
        final JsonNode json = request().body().asJson();
        try {
            final Set<Index> indexSet = indexRequestMapper.deserializeArray(json);
            indexService.addIndexes(indexSet);
            return ok();
        } catch (final DataFormatException e) {
            return badRequest(e.getMessage());
        } catch (IOException e) {
            return badRequest(e.getMessage());
        }
    }


    public Result deleteIndex(){
        final JsonNode json = request().body().asJson();
        try {
            final Index index = indexRequestMapper.deserialize(json);
            indexService.deleteIndex(index);
            return ok();
        } catch (final DataFormatException e) {
            return badRequest(e.getMessage());
        } catch (final NotFoundException e) {
            return badRequest(e.getMessage());
        } catch (IOException e) {
            return badRequest(e.getMessage());
        }
    }

    public Result getIndexes(){
        final Set<Index> indexes;
        try {
            indexes = indexService.findAll();
        } catch (IOException e) {
            return badRequest(e.getMessage());
        }
        final ObjectNode result = indexResponseMapper.serialize(indexes);
        return ok(result);
    }

    public Result beginIndexing(){
        try {
            pageIndexingService.beginIndexing();
        } catch (IOException e) {
            return badRequest(e.getMessage());
        }
        return ok();
    }




}
