package controllers;


import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.node.ObjectNode;
import enums.LoadStatus;
import javassist.NotFoundException;
import mappers.IndexReportResponseMapper;
import mappers.PagesResponseMapper;
import mappers.StatusResponseMapper;
import mappers.UrlRequestMapper;
import models.FilterParameter;
import models.Page;
import models.PageIndexReport;
import models.exceptions.AlreadyExistsException;
import models.requestmodels.UrlRequest;
import org.springframework.beans.factory.annotation.Autowired;
import play.Logger;
import play.mvc.BodyParser;
import play.mvc.Result;
import services.ExportService;
import services.PageService;

import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.zip.DataFormatException;

import static play.mvc.Controller.request;
import static play.mvc.Results.badRequest;
import static play.mvc.Results.ok;

@org.springframework.stereotype.Controller
public class PageController {

    private static final SimpleDateFormat DATE_PARSER = new SimpleDateFormat("dd-MM-yyyy HH:mm:ss");
    @Autowired
    private PageService pageService;
    @Autowired
    private ExportService exportService;
    @Autowired
    private UrlRequestMapper urlRequestMapper;
    @Autowired
    private StatusResponseMapper statusResponseMapper;
    @Autowired
    private PagesResponseMapper pagesResponseMapper;
    @Autowired
    private IndexReportResponseMapper indexReportResponseMapper;


    @BodyParser.Of(BodyParser.Json.class)
    public Result addPage() {
        final JsonNode json = request().body().asJson();
        try {
            final UrlRequest urlRequest = urlRequestMapper.deserialize(json);
            pageService.addPage(urlRequest.getUrl());
            return ok();
        } catch (final DataFormatException e) {
            return badRequest(e.getMessage());
        } catch (AlreadyExistsException e) {
            return badRequest(e.getMessage());
        } catch (IOException e) {
            return badRequest(e.getMessage());
        }
    }

    public Result getStatus() {
        final String urlStr = request().getHeader("url");
        if(urlStr == null) {
            return badRequest("Missing parameter [url]");
        } else {
            try {
                final URL url = new URL(urlStr);
                final LoadStatus loadStatus = pageService.getStatus(url);
                final ObjectNode result = statusResponseMapper.serialize(loadStatus);
                return ok(result);
            } catch (final MalformedURLException e) {
                return badRequest("Malformed URL " + urlStr);
            } catch (final NotFoundException e) {
                return badRequest("Url not found " + urlStr);
            } catch (IOException e) {
                return badRequest(e.getMessage());
            }

        }
    }

    @BodyParser.Of(BodyParser.Json.class)
    public Result deletePage() {
        final JsonNode json = request().body().asJson();
        try {
            final UrlRequest urlRequest = urlRequestMapper.deserialize(json);
            pageService.deletePage(urlRequest.getUrl());
            return ok();
        } catch (final DataFormatException e ) {
            return badRequest(e.getMessage());
        } catch (final NotFoundException e) {
            return badRequest(e.getMessage());
        }catch (final Exception e){
            e.printStackTrace();
            return badRequest(e.getLocalizedMessage());
        }
    }

    public Result getIndexReport(){
        final String urlStr = request().getHeader("url");
        if(urlStr == null) {
            return badRequest("Missing parameter [url]");
        } else {
            try {
                final URL url = new URL(urlStr);
                final PageIndexReport pageIndexReport = pageService.getPageIndexReport(url);
                final ObjectNode result = indexReportResponseMapper.serialize(pageIndexReport);
                result.put("url", urlStr);
                return ok(result);
            } catch (final MalformedURLException e) {
                return badRequest("Malformed URL " + urlStr);
            } catch (final NotFoundException e) {
                return badRequest("Url not found " + urlStr);
            } catch (IOException e) {
                return badRequest(e.getMessage());
            }
        }
    }

    public Result getFilteredPages(){
        final FilterParameter filterParameter = new FilterParameter();
        String parameter = request().getHeader("urlParameter");
        filterParameter.setUrlParameter(parameter);
        parameter = request().getHeader("titleParameter");
        filterParameter.setTitleParameter(parameter);
        parameter = request().getHeader("bodyParameter");
        filterParameter.setBodyParameter(parameter);
        parameter = request().getHeader("dateParameter");
        if(parameter!=null) {
            try {
                final Date dateParameter = DATE_PARSER.parse(parameter);
                filterParameter.setDateParameter(dateParameter);
            } catch (final ParseException e) {
                Logger.warn(e.getMessage());
                return badRequest("Wrong data format, try " + DATE_PARSER.toPattern());
            }
        }
        Logger.debug(filterParameter.toString());
        if(filterParameter.isEmpty()){
            return badRequest("No filter parameters");
        }
        final List<Page> pages;
        try {
            pages = pageService.getFilteredPages(filterParameter);
        } catch (IOException e) {
            return badRequest(e.getMessage());
        }
        final ObjectNode result = pagesResponseMapper.serialize(pages);
        return ok(result);
    }

    public Result exportToArchive() {
        final JsonNode json = request().body().asJson();
        try {
            final UrlRequest urlRequest = urlRequestMapper.deserialize(json);
            exportService.addToArchive(urlRequest.getUrl());
            return ok();
        } catch (final DataFormatException e) {
            return badRequest(e.getMessage());
        } catch (IOException e) {
            return badRequest(e.getMessage());
        } catch (NotFoundException e) {
            return badRequest(e.getMessage());
        }
    }

}