import play.PlayJava

name := """pageindexer"""

version := "1.0-SNAPSHOT"

libraryDependencies ++= Seq(
  javaCore,
  javaWs % "test",
  "org.springframework" % "spring-context" % "4.2.2.RELEASE",
  "org.springframework" % "spring-core" % "4.2.2.RELEASE",
  "org.springframework.data" % "spring-data-mongodb" % "1.8.0.RELEASE",
  "org.jsoup" % "jsoup" % "1.7.2",
  "mysql" % "mysql-connector-java" % "5.1.37",
  "org.springframework" % "spring-jdbc" % "4.2.3.RELEASE",
  "junit" % "junit" % "4.11",
  "org.springframework" % "spring-test" % "4.2.3.RELEASE"
)

lazy val root = (project in file(".")).enablePlugins(PlayJava)
